# Sistema base @tupaginaexpress
Sistema base para el desarrollo de aplicaciones del equipo de @tupaginaexpress.

## Tecnologías empleadas

Lenguaje de programación **PHP Versión 7**.

Framework **Cakephp Versión 3.X**.

Manejador de base de datos **Mysql Versión 5.7**.


## Herramientas necesarias

* Terminal o consola de línea de comandos.
	* Si utiliza linux sirve la consola que viene por defecto en el sistema.
	* Si utiliza windows puede utilizar [git bash](https://git-scm.com/downloads).
* Manejador de dependencias [composer](https://getcomposer.org/).
	* Es importante destacar que el composer debe estar instalado globalmente. En el caso de windows se instala global por defecto, si utiliza linux debe seguir los pasos explicados en el siguiente enlace. [Pasos instalación global](https://getcomposer.org/doc/00-intro.md#globally)

## Respaldo de la base de datos

El respaldo de la base de datos se encuentra en la siguiente ruta de este directorio **src/respaldos**, debe utlizar el de la fecha más reciente.


## Pasos para la instalación

1. Clonar el proyecto.
2. Comprobar que tenga instalado el composer ejecutando el siguiente comando:
```
composer -V
```
Si está instalado correctamente le devolverá un mensaje como el siguiente:
```
Composer 1.6.3 2018-01-31 16:28:17
```
3. Si ya tiene instalado el composer debe ubicarse en la raíz del proyecto y ejecutar el siguiente comando:
```
composer install
```
4. Duplicar el archivo **config/app.php.dev.ignore** y colocarle el nombre **app.php**, allí configuraremos la conexión a la base de datos en la llave **Datasources/Default** del arreglo.

## Usuarios para acceder al sistema

* Administrador
	* **Usuario:** administrador
	* **Contraseña:** 12345678
* Usuario básico
	* **Usuario:** operez
	* **Contraseña:** 12345678


## Tareas cotidianas y errores comunes

* A medida que vayamos desarrollando el sistema podemos utilizar nuevas dependencias que se instalan mediante composer, en caso de alguna falla porque el sistema no consiga algunas dependencias debemos ejecutar el siguiente comando para instalar las mismas.
```
composer update
```



