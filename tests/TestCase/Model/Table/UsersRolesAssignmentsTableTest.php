<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersRolesAssignmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersRolesAssignmentsTable Test Case
 */
class UsersRolesAssignmentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersRolesAssignmentsTable
     */
    public $UsersRolesAssignments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersRolesAssignments',
        'app.Users',
        'app.UsersRoles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersRolesAssignments') ? [] : ['className' => UsersRolesAssignmentsTable::class];
        $this->UsersRolesAssignments = TableRegistry::getTableLocator()->get('UsersRolesAssignments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersRolesAssignments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
