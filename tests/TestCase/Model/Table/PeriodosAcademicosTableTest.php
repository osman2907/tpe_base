<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PeriodosAcademicosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PeriodosAcademicosTable Test Case
 */
class PeriodosAcademicosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PeriodosAcademicosTable
     */
    public $PeriodosAcademicos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PeriodosAcademicos',
        'app.Descripcions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PeriodosAcademicos') ? [] : ['className' => PeriodosAcademicosTable::class];
        $this->PeriodosAcademicos = TableRegistry::getTableLocator()->get('PeriodosAcademicos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PeriodosAcademicos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
