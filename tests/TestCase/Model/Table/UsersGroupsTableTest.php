<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersGroupsTable Test Case
 */
class UsersGroupsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersGroupsTable
     */
    public $UsersGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersGroups',
        'app.Descripcions',
        'app.UsersRoles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersGroups') ? [] : ['className' => UsersGroupsTable::class];
        $this->UsersGroups = TableRegistry::getTableLocator()->get('UsersGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
