<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DescripcionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DescripcionsTable Test Case
 */
class DescripcionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DescripcionsTable
     */
    public $Descripcions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Descripcions',
        'app.Padres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Descripcions') ? [] : ['className' => DescripcionsTable::class];
        $this->Descripcions = TableRegistry::getTableLocator()->get('Descripcions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Descripcions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
