<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersRolesRoutesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersRolesRoutesTable Test Case
 */
class UsersRolesRoutesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersRolesRoutesTable
     */
    public $UsersRolesRoutes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersRolesRoutes',
        'app.UsersRoles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersRolesRoutes') ? [] : ['className' => UsersRolesRoutesTable::class];
        $this->UsersRolesRoutes = TableRegistry::getTableLocator()->get('UsersRolesRoutes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersRolesRoutes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
