-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2020 a las 19:48:56
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `documentos_academicos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descripcions`
--

CREATE TABLE `descripcions` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `padre_id` int(11) NOT NULL DEFAULT 0,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descripcions`
--

INSERT INTO `descripcions` (`id`, `nombre`, `padre_id`, `created`, `modified`) VALUES
(1, 'Estatus períodos Académicos', 0, NULL, NULL),
(2, 'Activo', 1, NULL, NULL),
(3, 'Inactivo', 1, NULL, NULL),
(10, 'Géneros', 0, NULL, NULL),
(11, 'Femenino', 10, NULL, NULL),
(12, 'Masculino', 10, NULL, NULL),
(100, 'Estatus de registro', 0, NULL, NULL),
(101, 'Activo', 100, NULL, NULL),
(102, 'Eliminado', 100, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_academicos`
--

CREATE TABLE `periodos_academicos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `estatus_id` int(11) NOT NULL DEFAULT 2,
  `status_id` int(11) NOT NULL DEFAULT 101,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodos_academicos`
--

INSERT INTO `periodos_academicos` (`id`, `nombre`, `fecha_inicio`, `fecha_fin`, `estatus_id`, `status_id`, `created`, `modified`) VALUES
(1, '2017-2018', '2019-09-16', '2020-07-17', 3, 101, NULL, NULL),
(2, '2018-2019', '2019-09-16', '2020-07-17', 3, 101, NULL, '2019-11-13 21:17:37'),
(3, '2019-2020', '2019-09-16', '2020-07-17', 2, 101, NULL, '2019-11-29 20:33:29'),
(7, '2020-2021', '2019-11-01', '2019-12-25', 3, 101, '2019-11-12 20:59:41', '2019-11-14 17:48:32'),
(8, '2021-2022', '2019-12-01', '2019-12-31', 3, 101, '2019-11-12 21:13:23', '2019-11-19 17:47:52'),
(9, '2022-2023', '2020-01-01', '2020-01-31', 3, 101, '2019-11-14 19:24:46', '2019-11-29 20:33:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `prefijo` varchar(5) NOT NULL,
  `primer_nombre` varchar(100) NOT NULL,
  `segundo_nombre` varchar(100) DEFAULT NULL,
  `primer_apellido` varchar(100) NOT NULL,
  `segundo_apellido` varchar(100) DEFAULT NULL,
  `genero_id` int(11) NOT NULL,
  `telefono1` varchar(100) NOT NULL,
  `telefono2` varchar(100) DEFAULT NULL,
  `correo_electronico` varchar(300) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 101,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'usuario',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `status_id` varchar(100) NOT NULL DEFAULT '101'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `contrasena`, `role`, `created`, `modified`, `nombre`, `apellido`, `status_id`) VALUES
(1, 'operez', '$2y$10$0FEsxCjmJbQkAWF3XdD9kODRsMVBdoIEY.UjEs9USwuqtpnSdHBW2', 'administrador', '2019-11-18 19:08:42', '2019-11-18 19:10:58', 'Osman', 'Pérez', '101'),
(2, 'ngonzalez', '$2y$10$ELpGgM87seaxJ6.RQzPzJ.9Akp1q8XF7LBawJuE9jbB6nr9o/l3jq', 'administrador', '2019-11-18 19:11:38', '2019-11-18 19:11:38', 'Norifer', 'González', '101'),
(3, 'cperez', '$2y$10$kbeNs/t9gb7H9wA7XvH7C.EX69LXAl1H/epFAK6pXeVZvRWkhv1ii', 'usuario', '2019-11-18 19:11:57', '2020-01-30 17:19:46', 'Cruz', 'Pérez', '101');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 101,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `nombre`, `status_id`, `created`, `modified`) VALUES
(1, 'Personal Técnico', 101, '2019-12-04 19:32:46', '2020-01-24 20:15:47'),
(2, 'Vendedores', 101, '2020-01-28 18:11:46', '2020-01-28 18:12:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 101,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `users_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_roles`
--

INSERT INTO `users_roles` (`id`, `nombre`, `status_id`, `created`, `modified`, `users_group_id`) VALUES
(1, 'Administrador', 101, '2019-12-04 19:59:40', '2020-01-28 17:16:34', 1),
(2, 'Cajero', 101, '2020-01-28 20:20:48', '2020-01-28 20:21:33', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles_assignments`
--

CREATE TABLE `users_roles_assignments` (
  `user_id` int(11) NOT NULL,
  `users_role_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles_routes`
--

CREATE TABLE `users_roles_routes` (
  `users_role_id` int(11) NOT NULL,
  `plugin` varchar(100) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `descripcions`
--
ALTER TABLE `descripcions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periodos_academicos_fk` (`estatus_id`),
  ADD KEY `periodos_academicos_fk_1` (`status_id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personas_un` (`cedula`),
  ADD KEY `personas_fk` (`genero_id`),
  ADD KEY `personas_fk_1` (`status_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_groups_fk` (`status_id`);

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_roles_fk` (`status_id`),
  ADD KEY `users_roles_fk_1` (`users_group_id`);

--
-- Indices de la tabla `users_roles_assignments`
--
ALTER TABLE `users_roles_assignments`
  ADD PRIMARY KEY (`user_id`,`users_role_id`),
  ADD KEY `users_roles_assignments_fk_1` (`users_role_id`);

--
-- Indices de la tabla `users_roles_routes`
--
ALTER TABLE `users_roles_routes`
  ADD PRIMARY KEY (`users_role_id`,`plugin`,`controller`,`action`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `descripcions`
--
ALTER TABLE `descripcions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  ADD CONSTRAINT `periodos_academicos_fk` FOREIGN KEY (`estatus_id`) REFERENCES `descripcions` (`id`),
  ADD CONSTRAINT `periodos_academicos_fk_1` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_fk` FOREIGN KEY (`genero_id`) REFERENCES `descripcions` (`id`),
  ADD CONSTRAINT `personas_fk_1` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_fk` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `users_roles_fk` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`),
  ADD CONSTRAINT `users_roles_fk_1` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`);

--
-- Filtros para la tabla `users_roles_assignments`
--
ALTER TABLE `users_roles_assignments`
  ADD CONSTRAINT `users_roles_assignments_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_roles_assignments_fk_1` FOREIGN KEY (`users_role_id`) REFERENCES `users_roles` (`id`);

--
-- Filtros para la tabla `users_roles_routes`
--
ALTER TABLE `users_roles_routes`
  ADD CONSTRAINT `users_roles_routes_fk` FOREIGN KEY (`users_role_id`) REFERENCES `users_roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
