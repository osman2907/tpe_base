-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-06-2020 a las 20:55:29
-- Versión del servidor: 5.7.30
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tpe_base`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`id`, `persona_id`, `empresa_id`, `status_id`, `created`, `modified`) VALUES
(1, 1, NULL, 101, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, NULL, 1, 101, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descripcions`
--

CREATE TABLE `descripcions` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `padre_id` int(11) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descripcions`
--

INSERT INTO `descripcions` (`id`, `nombre`, `padre_id`, `created`, `modified`) VALUES
(1, 'Estatus períodos Académicos', 0, NULL, NULL),
(2, 'Activo', 1, NULL, NULL),
(3, 'Inactivo', 1, NULL, NULL),
(10, 'Géneros', 0, NULL, NULL),
(11, 'Femenino', 10, NULL, NULL),
(12, 'Masculino', 10, NULL, NULL),
(15, 'Tipos de empresa', 0, NULL, NULL),
(16, 'Empresa privada', 15, NULL, NULL),
(17, 'Institución gubernamental', 15, NULL, NULL),
(18, 'Fundación benéfica', 15, NULL, NULL),
(25, 'Presentaciones', 0, NULL, NULL),
(26, 'Detallado', 25, NULL, NULL),
(27, 'Caja', 25, NULL, NULL),
(100, 'Estatus de registro', 0, NULL, NULL),
(101, 'Activo', 100, NULL, NULL),
(102, 'Eliminado', 100, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `prefijo` varchar(10) NOT NULL,
  `rif` varchar(20) NOT NULL,
  `razon_social` varchar(300) NOT NULL,
  `objeto` text,
  `tipo_id` int(11) NOT NULL,
  `telefono1` varchar(100) NOT NULL,
  `telefono2` varchar(100) DEFAULT NULL,
  `correo_electronico` varchar(300) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `prefijo`, `rif`, `razon_social`, `objeto`, `tipo_id`, `telefono1`, `telefono2`, `correo_electronico`, `status_id`, `created`, `modified`) VALUES
(1, 'j', '123456789', 'Tu pagina express', NULL, 16, '04263063803', '', 'soporte@tupaginaexpress.com.ve', 101, '2020-02-22 11:08:39', '2020-04-03 15:35:44'),
(2, 'j', '987654321', 'Los totumitos C.A', NULL, 16, '02128788221', '', 'totumitos@totumitos.com', 101, '2020-04-03 15:29:09', '2020-04-03 15:40:07'),
(3, 'g', '111166662', 'Los morochos', NULL, 18, '04142307460', '', 'losmorochos@gmail.com', 101, '2020-04-03 15:38:53', '2020-04-03 15:39:31'),
(4, 'j', '123456781', 'inversiones los totumitos', NULL, 16, '04129368030', '', 'inversionest@gmail.com', 101, '2020-04-06 13:45:51', '2020-04-06 13:45:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_academicos`
--

CREATE TABLE `periodos_academicos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `estatus_id` int(11) NOT NULL DEFAULT '2',
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodos_academicos`
--

INSERT INTO `periodos_academicos` (`id`, `nombre`, `fecha_inicio`, `fecha_fin`, `estatus_id`, `status_id`, `created`, `modified`) VALUES
(1, '2017-2018', '2019-09-16', '2020-07-17', 3, 101, NULL, NULL),
(2, '2018-2019', '2019-09-16', '2020-07-17', 3, 101, NULL, '2019-11-13 21:17:37'),
(3, '2019-2020', '2019-09-16', '2020-07-17', 2, 101, NULL, '2019-11-29 20:33:29'),
(7, '2020-2021', '2019-11-01', '2019-12-25', 3, 101, '2019-11-12 20:59:41', '2019-11-14 17:48:32'),
(8, '2021-2022', '2019-12-01', '2019-12-31', 3, 101, '2019-11-12 21:13:23', '2019-11-19 17:47:52'),
(9, '2022-2023', '2020-01-01', '2020-01-31', 3, 101, '2019-11-14 19:24:46', '2019-11-29 20:33:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `prefijo` varchar(5) NOT NULL,
  `primer_nombre` varchar(100) NOT NULL,
  `segundo_nombre` varchar(100) DEFAULT NULL,
  `primer_apellido` varchar(100) NOT NULL,
  `segundo_apellido` varchar(100) DEFAULT NULL,
  `genero_id` int(11) NOT NULL,
  `telefono1` varchar(100) NOT NULL,
  `telefono2` varchar(100) DEFAULT NULL,
  `correo_electronico` varchar(300) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `prefijo`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `genero_id`, `telefono1`, `telefono2`, `correo_electronico`, `status_id`, `created`, `modified`) VALUES
(1, '19255285', 'v', 'Osman', 'Orlando', 'Pérez', 'Martínez', 12, '04168016664', '04263063803', 'osman2907@gmail.com', 101, '2020-02-05 14:44:07', '2020-05-20 15:30:27'),
(2, '12345678', 'v', 'Administrador', '', 'Sistema', '', 12, '04263063803', '', 'osman2907@gmail.com', 101, '2020-06-13 21:56:01', '2020-06-13 21:56:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedors`
--

CREATE TABLE `proveedors` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedors`
--

INSERT INTO `proveedors` (`id`, `persona_id`, `empresa_id`, `status_id`, `created`, `modified`) VALUES
(1, NULL, 1, 101, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'usuario',
  `persona_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `status_id` varchar(100) NOT NULL DEFAULT '101'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `contrasena`, `role`, `persona_id`, `created`, `modified`, `status_id`) VALUES
(1, 'operez', '$2y$10$TohUvIhwgT/78nozNbfDq.q4zCwB/6Pv9bs7z7FGnbmarm.gjUjfi', 'usuario', 1, '2019-11-18 19:08:42', '2020-06-13 21:56:32', '101'),
(2, 'administrador', '$2y$10$bb9PN1x5FUnNsUxJAdR9I.hcNHs8zeCcX4coCrI0xecXL3naTqZPi', 'administrador', 2, '2020-06-13 21:56:11', '2020-06-13 21:56:11', '101');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `nombre`, `status_id`, `created`, `modified`) VALUES
(1, 'Personal Técnico', 101, '2019-12-04 19:32:46', '2020-05-29 15:20:27'),
(2, 'Vendedores', 101, '2020-01-28 18:11:46', '2020-03-16 22:35:42'),
(3, 'Supervisores', 101, '2020-02-24 11:49:22', '2020-04-06 14:32:22'),
(4, 'Consignatarios', 101, '2020-04-02 12:48:45', '2020-05-29 15:16:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '101',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `users_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_roles`
--

INSERT INTO `users_roles` (`id`, `nombre`, `status_id`, `created`, `modified`, `users_group_id`) VALUES
(1, 'Administrador', 101, '2019-12-04 19:59:40', '2020-04-02 12:28:11', 1),
(2, 'Cajero', 101, '2020-01-28 20:20:48', '2020-03-16 22:34:42', 2),
(3, 'Supervisor de ventas', 101, '2020-02-24 11:49:37', '2020-04-02 12:28:17', 3),
(4, 'Gerentes', 101, '2020-04-02 12:17:09', '2020-05-29 14:45:29', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles_assignments`
--

CREATE TABLE `users_roles_assignments` (
  `user_id` int(11) NOT NULL,
  `users_role_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_roles_assignments`
--

INSERT INTO `users_roles_assignments` (`user_id`, `users_role_id`, `created`, `modified`) VALUES
(1, 3, '2020-06-13 21:56:49', '2020-06-13 21:56:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles_routes`
--

CREATE TABLE `users_roles_routes` (
  `users_role_id` int(11) NOT NULL,
  `plugin` varchar(100) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_roles_routes`
--

INSERT INTO `users_roles_routes` (`users_role_id`, `plugin`, `controller`, `action`, `created`, `modified`) VALUES
(1, 'App', 'Pages', 'display', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'Pages', 'inicio', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'Pages', 'login', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'Pages', 'logout', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'activarPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'actualizarPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'crearPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'desactivarPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'editarPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'eliminarPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'index', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'nuevoPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'reciclarPeriodoAcademico', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(1, 'App', 'PeriodosAcademicos', 'view', '2020-04-02 11:46:51', '2020-04-02 11:46:51'),
(2, 'Personas', 'Personas', 'consultar', '2020-05-20 15:30:24', '2020-05-20 15:30:24'),
(2, 'Personas', 'Personas', 'eliminar', '2020-05-20 15:30:24', '2020-05-20 15:30:24'),
(2, 'Personas', 'Personas', 'listar', '2020-05-20 15:30:24', '2020-05-20 15:30:24'),
(2, 'Personas', 'Personas', 'reciclar', '2020-05-20 15:30:24', '2020-05-20 15:30:24'),
(3, 'App', 'UsersGroups', 'consultar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersGroups', 'editar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersGroups', 'eliminar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersGroups', 'listar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersGroups', 'reciclar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersGroups', 'registrar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersRoles', 'consultar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(3, 'App', 'UsersRoles', 'listar', '2020-06-13 21:58:54', '2020-06-13 21:58:54'),
(4, 'App', 'Pages', 'display', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'App', 'Pages', 'inicio', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'App', 'Pages', 'login', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'App', 'Pages', 'logout', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'Personas', 'Personas', 'consultar', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'Personas', 'Personas', 'editar', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'Personas', 'Personas', 'eliminar', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'Personas', 'Personas', 'listar', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'Personas', 'Personas', 'reciclar', '2020-04-20 13:09:03', '2020-04-20 13:09:03'),
(4, 'Personas', 'Personas', 'registrar', '2020-04-20 13:09:03', '2020-04-20 13:09:03');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persona_id` (`persona_id`),
  ADD UNIQUE KEY `empresa_id` (`empresa_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `descripcions`
--
ALTER TABLE `descripcions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `empresas_UN` (`rif`),
  ADD KEY `empresas_FK` (`tipo_id`);

--
-- Indices de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periodos_academicos_fk` (`estatus_id`),
  ADD KEY `periodos_academicos_fk_1` (`status_id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personas_un` (`cedula`),
  ADD KEY `personas_fk` (`genero_id`),
  ADD KEY `personas_fk_1` (`status_id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `proveedors`
--
ALTER TABLE `proveedors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persona_id` (`persona_id`),
  ADD UNIQUE KEY `empresa_id` (`empresa_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `persona_id` (`persona_id`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_groups_fk` (`status_id`);

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_roles_fk` (`status_id`),
  ADD KEY `users_roles_fk_1` (`users_group_id`);

--
-- Indices de la tabla `users_roles_assignments`
--
ALTER TABLE `users_roles_assignments`
  ADD PRIMARY KEY (`user_id`,`users_role_id`);

--
-- Indices de la tabla `users_roles_routes`
--
ALTER TABLE `users_roles_routes`
  ADD PRIMARY KEY (`users_role_id`,`plugin`,`controller`,`action`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `descripcions`
--
ALTER TABLE `descripcions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedors`
--
ALTER TABLE `proveedors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`),
  ADD CONSTRAINT `clients_ibfk_2` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`),
  ADD CONSTRAINT `clients_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `empresas_FK` FOREIGN KEY (`tipo_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  ADD CONSTRAINT `periodos_academicos_fk` FOREIGN KEY (`estatus_id`) REFERENCES `descripcions` (`id`),
  ADD CONSTRAINT `periodos_academicos_fk_1` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_fk` FOREIGN KEY (`genero_id`) REFERENCES `descripcions` (`id`),
  ADD CONSTRAINT `personas_fk_1` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `proveedors`
--
ALTER TABLE `proveedors`
  ADD CONSTRAINT `proveedors_ibfk_1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`),
  ADD CONSTRAINT `proveedors_ibfk_2` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`),
  ADD CONSTRAINT `proveedors_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_fk` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`);

--
-- Filtros para la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `users_roles_fk` FOREIGN KEY (`status_id`) REFERENCES `descripcions` (`id`),
  ADD CONSTRAINT `users_roles_fk_1` FOREIGN KEY (`users_group_id`) REFERENCES `users_groups` (`id`);

--
-- Filtros para la tabla `users_roles_assignments`
--
ALTER TABLE `users_roles_assignments`
  ADD CONSTRAINT `users_roles_assignments_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users_roles_routes`
--
ALTER TABLE `users_roles_routes`
  ADD CONSTRAINT `users_roles_routes_fk` FOREIGN KEY (`users_role_id`) REFERENCES `users_roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
