<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Sistema base @tupaginaexpress</title>

    <!-- Fontfaces CSS-->
    <?= $this->Html->css('cooladmin/font-face.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/font-awesome-4.7/css/font-awesome.min.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/font-awesome-5/css/fontawesome-all.min.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/mdi-font/css/material-design-iconic-font.min.css') ?>

    <!-- Bootstrap CSS-->
    <?= $this->Html->css('../vendor/cooladmin/bootstrap-4.1/bootstrap.min.css') ?>

    <!-- Vendor CSS-->
    <?= $this->Html->css('../vendor/cooladmin/animsition/animsition.min.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/wow/animate.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/css-hamburgers/hamburgers.min.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/slick/slick.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/select2/select2.min.css') ?>
    <?= $this->Html->css('../vendor/cooladmin/perfect-scrollbar/perfect-scrollbar.css') ?>


    <!-- Main CSS-->
    <?= $this->Html->css('cooladmin/theme.css') ?>


    <!-- Jquery JS-->
    <?= $this->Html->script('../vendor/cooladmin/jquery-3.2.1.min.js') ?>


    <!-- SweetAlert2-->
    <?= $this->Html->script('sweetalert2/sweetalert2.min.js') ?>
    <?= $this->Html->css('sweetalert2/sweetalert2.min.css') ?>


    <!-- Jquery UI-->
    <?= $this->Html->css('jquery-ui/jquery-ui.css') ?>
    <?= $this->Html->script('jquery-ui/jquery-ui.js') ?>

    <?= $this->Html->script('main.js') ?>

</head>

<body class="animsition2">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="<?= $this->Url->build(["plugin"=>null,"controller" => "Pages","action" => "inicio"]);  ?>">
                            <?= $this->Html->image('logo512.png',['style'=>"width:160px;"]) ?>
                        </a>
                        
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                        
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        
                        <?php if(!isset($current_user)){ ?>
                            <li><a href="<?= $this->Url->build(["controller" => "Pages","action" => "login"]);  ?>">Inicio de sesión</a></li>
                            <li><a href="<?= $this->Url->build(["controller" => "Pages","action" => "register"]);  ?>">Crear usuario</a></li>
                            <li><a href="<?= $this->Url->build(["controller" => "Pages","action" => "recovery"]);  ?>">Recuperar contraseña</a></li>
                        <?php } ?>

                        <?php if(isset($current_user)){ ?>
                            <li class="has-sub">
                                <?php if(isset($permisos['personas']['personas']) || isset($permisos['empresas']['empresas'])){ ?>

                                    <a class="js-arrow" href="#"><i class="fas fa-cog"></i>Configuración</a>
                                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                        
                                        <?php if(isset($permisos['personas']['personas'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>"personas", "controller" => "personas","action" => "listar"]);  ?>">Personas</a></li>
                                        <?php } ?>

                                        <?php if(isset($permisos['empresas']['empresas'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>"empresas", "controller" => "empresas","action" => "listar"]);  ?>">Empresas</a></li>
                                        <?php } ?>
                                    </ul>

                                <?php } ?>
                            </li>

                            <li class="has-sub">
                                <?php if(isset($permisos['app']['users']) || isset($permisos['app']['usersroles']) || isset($permisos['app']['usersgroups'])){ ?>

                                    <a class="js-arrow" href="#"><i class="fas fa-lock"></i>Permisología</a>
                                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">

                                        <?php if(isset($permisos['app']['users'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>false, "controller" => "users","action" => "listar"]);  ?>" class="active">Usuarios</a></li>
                                        <?php } ?>

                                        <?php if(isset($permisos['app']['usersroles'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>false, "controller" => "usersRoles","action" => "listar"]);  ?>">Roles</a></li>
                                        <?php } ?>

                                        <?php if(isset($permisos['app']['usersgroups'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>false, "controller" => "UsersGroups","action" => "listar"]);  ?>">Grupos</a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?= $this->Url->build(["plugin"=>null,"controller" => "Pages","action" => "inicio"]);  ?>">
                    <?= $this->Html->image('logo512.png',['style'=>"width:170px;"]) ?>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php if(!isset($current_user)){ ?>
                            <li><a href="<?= $this->Url->build(["controller" => "Pages", "action" => "login"]);  ?>">Inicio de sesión</a></li>
                            <li><a href="<?= $this->Url->build(["controller" => "Pages", "action" => "register"]);  ?>">Crear usuario</a></li>
                            <li><a href="<?= $this->Url->build(["controller" => "Pages", "action" => "recovery"]);  ?>">Recuperar Contraseña</a></li>
                        <?php } ?>

                        <?php if(isset($current_user)){ ?>
                            
                            <?php if(isset($permisos['personas']['personas']) || isset($permisos['empresas']['empresas'])){ ?>
                                <li class="has-sub">
                                    <a class="js-arrow" href="#">
                                        <i class="fas fa-cog"></i>Configuración</a>
                                    <ul class="list-unstyled navbar__sub-list js-sub-list">

                                        <?php if(isset($permisos['personas']['personas'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>"personas", "controller" => "personas","action" => "listar"]);  ?>">Personas</a></li>
                                        <?php } ?>

                                        <?php if(isset($permisos['empresas']['empresas'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>"empresas", "controller" => "empresas","action" => "listar"]);  ?>">Empresas</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                            

                            <?php if(isset($permisos['app']['users']) || isset($permisos['app']['usersroles']) || isset($permisos['app']['usersgroups'])){ ?>

                                <li class="has-sub">
                                    <a class="js-arrow" href="#">
                                        <i class="fas fa-lock"></i>Permisología</a>
                                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                                        <?php if(isset($permisos['app']['users'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>false, "controller" => "users","action" => "listar"]);  ?>" class="active">Usuarios</a></li>
                                        <?php } ?>

                                        <?php if(isset($permisos['app']['usersroles'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>false, "controller" => "usersRoles","action" => "listar"]);  ?>">Roles</a></li>
                                        <?php } ?>

                                        <?php if(isset($permisos['app']['usersgroups'])){ ?>
                                            <li><a href="<?= $this->Url->build(["plugin"=>false, "controller" => "UsersGroups","action" => "listar"]);  ?>">Grupos</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>

                            <?php } ?>

                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <?php if(isset($current_user)){ ?>
                                <?= $this->Form->create(null,['id'=>'formulario-busqueda','class'=>'form-header']); ?>
                                    <!--<input class="au-input au-input--xl" type="text" name="search" placeholder="Buscar" />
                                    <button class="au-btn--submit" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>-->
                                <?= $this->Form->end(); ?>
                            <?php } ?>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <?php if(isset($current_user)){ ?>
                                        <div class="noti__item js-item-menu">
                                            <i class="zmdi zmdi-comment-more"></i>
                                            <span class="quantity">1</span>
                                            <div class="mess-dropdown js-dropdown">
                                                <div class="mess__title">
                                                    <p>Tienes 2 mensajes nuevos</p>
                                                </div>
                                                <div class="mess__item">
                                                    <div class="image img-cir img-40">
                                                        <?= $this->Html->image('cooladmin/icon/avatar-06.jpg',['alt'=>'Michelle Moreno']) ?>
                                                    </div>
                                                    <div class="content">
                                                        <h6>Michelle Moreno</h6>
                                                        <p>Have sent a photo</p>
                                                        <span class="time">3 min ago</span>
                                                    </div>
                                                </div>
                                                <div class="mess__item">
                                                    <div class="image img-cir img-40">
                                                        <?= $this->Html->image('cooladmin/icon/avatar-04.jpg',['alt'=>'Diane Myers']) ?>
                                                    </div>
                                                    <div class="content">
                                                        <h6>Diane Myers</h6>
                                                        <p>You are now connected on message</p>
                                                        <span class="time">Yesterday</span>
                                                    </div>
                                                </div>
                                                <div class="mess__footer">
                                                    <a href="#">Ver todos los mensajes</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <!-- <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity">1</span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>Tienes 3 emails nuevos</p>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <?= $this->Html->image('cooladmin/icon/avatar-06.jpg',['alt'=>'Diane Myers']) ?>
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, 3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <?= $this->Html->image('cooladmin/icon/avatar-05.jpg',['alt'=>'Diane Myers']) ?>
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <?= $this->Html->image('cooladmin/icon/avatar-04.jpg',['alt'=>'Diane Myers']) ?>
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, April 12,,2018</span>
                                                </div>
                                            </div>
                                            <div class="email__footer">
                                                <a href="#">Ver todos los emails</a>
                                            </div>
                                        </div>
                                    </div> -->

                                    <?php if(isset($current_user)){ ?>
                                        <div class="noti__item js-item-menu">
                                            <i class="zmdi zmdi-notifications"></i>
                                            <span class="quantity">3</span>
                                            <div class="notifi-dropdown js-dropdown">
                                                <div class="notifi__title">
                                                    <p>Tienes 3 notificaciones</p>
                                                </div>
                                                <div class="notifi__item">
                                                    <div class="bg-c1 img-cir img-40">
                                                        <i class="zmdi zmdi-email-open"></i>
                                                    </div>
                                                    <div class="content">
                                                        <p>You got a email notification</p>
                                                        <span class="date">April 12, 2018 06:50</span>
                                                    </div>
                                                </div>
                                                <div class="notifi__item">
                                                    <div class="bg-c2 img-cir img-40">
                                                        <i class="zmdi zmdi-account-box"></i>
                                                    </div>
                                                    <div class="content">
                                                        <p>Your account has been blocked</p>
                                                        <span class="date">April 12, 2018 06:50</span>
                                                    </div>
                                                </div>
                                                <div class="notifi__item">
                                                    <div class="bg-c3 img-cir img-40">
                                                        <i class="zmdi zmdi-file-text"></i>
                                                    </div>
                                                    <div class="content">
                                                        <p>You got a new file</p>
                                                        <span class="date">April 12, 2018 06:50</span>
                                                    </div>
                                                </div>
                                                <div class="notifi__footer">
                                                    <a href="#">All notifications</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if(isset($current_user)){ ?>
                                    <div class="account-wrap">
                                        <div class="account-item clearfix js-item-menu">
                                            <div class="image">
                                                <?= $this->Html->image('cooladmin/icon/avatar-01.jpg',['alt'=>'Diane Myers']) ?>
                                            </div>
                                            <div class="content">
                                                <a class="js-acc-btn" href="#"><?= $_SESSION['Auth']['User']['nombre']." ".$_SESSION['Auth']['User']['apellido'] ?></a>
                                            </div>
                                            <div class="account-dropdown js-dropdown">
                                                <div class="info clearfix">
                                                    <div class="image">
                                                        <a href="#">
                                                            <?= $this->Html->image('cooladmin/icon/avatar-01.jpg',['alt'=>'Diane Myers']) ?>
                                                        </a>
                                                    </div>
                                                    <div class="content">
                                                        <h5 class="name">
                                                            <a href="#"><?= $_SESSION['Auth']['User']['nombre']." ".$_SESSION['Auth']['User']['apellido'] ?></a>
                                                        </h5>
                                                        <span class="email"><?= $_SESSION['Auth']['User']['correo_electronico'] ?></span>
                                                    </div>
                                                </div>
                                                <div class="account-dropdown__body">
                                                    <div class="account-dropdown__item">
                                                        <a href="#">
                                                            <i class="zmdi zmdi-account"></i>Perfil</a>
                                                    </div>
                                                    <div class="account-dropdown__item">
                                                        <a href="#">
                                                            <i class="zmdi zmdi-settings"></i>Configuración</a>
                                                    </div>
                                                </div>
                                                <div class="account-dropdown__footer">
                                                    <a id="link-cerrar-sesion" href="<?= $this->Url->build(["plugin"=>false,"controller" => "Pages","action" => "logout"]);  ?>">
                                                        <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <?php if(isset($_SESSION['Flash']['auth'])){ ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger" style="width: 100%">
                                        <?= $_SESSION['Flash']['auth'][0]['message'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?= $this->fetch('content') ?>
                        <?php //debug($permisos); ?>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            
            <div id="modal-container"></div>
        </div><!-- END PAGE CONTAINER-->

    </div>

    <!-- Bootstrap JS-->
    <?= $this->Html->script('../vendor/cooladmin/bootstrap-4.1/popper.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/bootstrap-4.1/bootstrap.min.js') ?>
    <!-- Vendor JS       -->
    <?= $this->Html->script('../vendor/cooladmin/slick/slick.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/wow/wow.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/animsition/animsition.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/counter-up/jquery.waypoints.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/counter-up/jquery.counterup.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/circle-progress/circle-progress.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/perfect-scrollbar/perfect-scrollbar.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/chartjs/Chart.bundle.min.js') ?>
    <?= $this->Html->script('../vendor/cooladmin/select2/select2.min.js') ?>

    <!-- Main JS-->
    <?= $this->Html->script('cooladmin/main.js') ?>
</body>
<div class="page-loader hide"><div class="page-loader__spin"></div></div>

</html>
<!-- end document-->
