<?= $this->Form->create($user,['id'=>'form-nuevo-usuario-identificacion']) ?>
	<?= $this->Form->hidden('accion',['value'=>'registrar_identificacion']);   ?>

	<div class="alert alert-info">
		Por favor ingrese los datos de identificación, luego presione el botón <b>siguiente</b> para validar la existencia de la persona en la base de datos.
	</div>

	<div class="row">
	 	<div class="col-12 col-sm-6 col-md-4">
	 		<div class="input">
		        <?php
		        echo $this->Form->label("prefijo","Prefijo",['style'=>'display: inline']); 
		        echo $this->Form->select('prefijo',$prefijosList,['class'=>'form-control','style'=>'width:100%','id'=>'select-prefijo-identificacion']); 
		        ?>
		    </div>
	    </div>

	    <div class="col-12 col-sm-6 col-md-4">
	    	<div class="input">
		        <?php
		        echo $this->Form->label("cedula","Cédula",['style'=>'display: inline']); 
		        echo $this->Form->text('cedula',['class'=>'form-control solo-numero','style'=>'width:100%','id'=>'txt-cedula-identificacion']); 
		        ?>
		    </div>
	    </div>

	</div>
<?= $this->Form->end() ?>


<script type="text/javascript">
	$("document").ready(function(){

		$("#form-nuevo-usuario-identificacion").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				formularioRegistrarPersona();
			}
		});

		$("#form-nuevo-usuario-identificacion").submit(function(){
			return false;
		});

		$("#btn-siguiente").click(function(){
			formularioRegistrarPersona();
			return false;
		});

	});



	function formularioPersonas(data) {
		envio={};
		envio.accion='formulario_personas';
		if(data.datos){
			envio.persona_id=data.datos.persona_id;
		}else{
			envio.prefijo=$("#select-prefijo-identificacion").val();
			envio.cedula=$("#txt-cedula-identificacion").val();
		}

		$.ajax({
            type: "POST",
            url: "<?= $this->Url->build(["action" => "registrar"]); ?>",
            data:envio,
            dataType: "html",
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
            	$(".page-loader").addClass('hide');
            	$(".modal-body").html(data);
            	$("#btn-siguiente").unbind();
            	$("#btn-anterior").removeClass('hide');
            	$("#btn-siguiente").removeClass('hide');
            },
            error: function (status, error, datos){
            	$(".page-loader").addClass('hide');
            	Swal.fire({
		            icon: 'error',
		            title: '',
		            text: 'Error al consultar, por favor intente nuevamente',
		            footer: ''
		        });
            }
        });
	}


	function formularioRegistrarPersona(){
			valor=$("#txt-cedula-identificacion").val().trim();

			if(valor == ''){
				Swal.fire({
		            icon: 'error',
		            title: '',
		            text: 'Por favor ingrese el número de cédula',
		            footer: '',
		        });
				return false;
			}

			var data=$("#form-nuevo-usuario-identificacion").serialize();
			$.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["action" => "registrar"]); ?>",
                data:data,
                dataType: "json",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                	$(".page-loader").addClass('hide');
                	//$(".close").trigger("click");
                	
                	if(data.result){
                		formularioPersonas(data);
                	}else{
                		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: data.mensaje,
				            footer: ''
				        });
                	}
                },
                error: function (status, error, datos){
                	$(".page-loader").addClass('hide');
                	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al registrar, por favor intente nuevamente',
			            footer: ''
			        });
                }
            });
			return false;
		}
</script>
