<div class="modal fade" id="modal-editar-usuario" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<?= $this->Form->create($user,['id'=>'form-editar-usuario']) ?>
		<?= $this->Form->control('id'); ?>
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Editar usuario</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body contenedor-formulario">
						<div class="row">

							<div class="col-12 col-sm-6 col-md-4">
	              				<?= $this->Form->control('cedula',['class'=>'form-control','readonly'=>'readonly','label'=>'Cédula']); ?>
	                		</div>

	                		<div class="col-12 col-sm-6 col-md-4">
	              				<?= $this->Form->control('primer_nombre',['class'=>'form-control','readonly'=>'readonly','label'=>'Nombre']); ?>
	                		</div>

	                		<div class="col-12 col-sm-6 col-md-4">
	              				<?= $this->Form->control('primer_apellido',['class'=>'form-control','readonly'=>'readonly','label'=>'Apellido']); ?>
	                		</div>

	              			<div class="col-12 col-sm-6 col-md-4">
	              				<?= $this->Form->control('usuario',['class'=>'form-control']); ?>
	                		</div>

	                		

	                		<div class="col-12 col-sm-6 col-md-4">
	              				<?= $this->Form->control('role',['options'=>$roleList, 'class'=>'form-control','label'=>'Tipo']); ?>
	                		</div>

	                		<div class="col-12 col-sm-6 col-md-4">
	              				<?= $this->Form->control('status_id', ['type' => 'hidden', 'class'=>'form-control']); ?>
	                		</div>

	                	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<?= $this->Form->button("Editar",['id'=>'btn-editar','class'=>'btn btn-success']); ?>
				</div>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("document").ready(function(){

		$("#modal-editar-usuario").modal('show');
  
		$("#btn-editar").click(function(){
			data=$("#form-editar-usuario").serialize();
			$.ajax({
	            type: "POST",
	            url: "<?= $this->Url->build(["action" => "editar",$user->id]); ?>",
	            data:data,
	            dataType: "json",
	            beforeSend: function(xhr){
	                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
	                $(".page-loader").removeClass('hide');
	            },
	            success: function(data){
	            	$(".page-loader").addClass('hide');
	            	$("label[class='error']").remove();
                	if(data.errors){
                		errores=data.errors;
                		$.each(errores, function(i,mensaje){
                			$("[name='"+i+"']").parent().append('<label class="error">'+mensaje+'</label>');
                		});
                		return false;
                	}
	            	$(".close").trigger("click");

	            	if(data.result){
	            		Swal.fire({
				            icon: 'success',
				            title: data.mensaje,
				            text: '',
				            footer: ''
				        }).then((result) => {
				        	if(result.value){
				        		selPage=$(".page-number[tabindex='-1']");
				        		if(selPage.length === 0){
				        			$("#buscar").trigger('click');
				        		}else{
				        			selPage.trigger('click');
				        		}
				        	}
				        });
	            	}else{
	            		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: data.mensaje,
				            footer: ''
				        });
	            	}
	            },
	            error: function (status, error, datos){
	            	$(".page-loader").addClass('hide');
	            	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al actualizar, por favor intente nuevamente',
			            footer: ''
			        });
	            }
	        });
			return false;
		});
	});

</script>