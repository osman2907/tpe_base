<div class="modal fade" id="modal-asignar-roles" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<?= $this->Form->create($user,['id'=>'form-asignar-roles']) ?>
		<?= $this->Form->control('id'); ?>
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Asignar roles</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body contenedor-formulario">
                    <div class="container-consulta1">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Cédula</div>
                                <div class="contenido"><?= strtoupper($user->persona->prefijo)."-".$user->persona->cedula; ?></div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Nombres</div>
                                <div class="contenido"><?= $user->persona->primer_nombre." ".$user->persona->segundo_nombre; ?></div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Apellidos</div>
                                <div class="contenido"><?= $user->persona->primer_apellido." ".$user->persona->segundo_apellido; ?></div>
                            </div>
                        
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Usuario</div>
                                <div class="contenido"><?= $user->usuario; ?></div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Tipo</div>
                                <div class="contenido"><?= ucwords($user->role); ?></div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Eliminado</div>
                                <div class="contenido">
                                    <?php
                                    $clase=$user->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                                    ?>
                                    <h4><span class="<?= $clase ?>"><?= $user->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>

                	<div class="alert-warning" style="padding: 5px; margin-top: 20px;">
                        <div class="row">
                            <div class="col-sm-3">
                                <label><b>Buscar:</b></label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" id="txt-buscar" class="form-control" placeholder="Buscar...">
                            </div>
                            <!--<div class="col-sm-3 offset-sm-1">
                                <label>
                                    <input type="checkbox" id="check-todos" class="form-check-input">
                                    <b>Seleccionar todos</b>
                                </label>
                            </div>-->
                        </div>
                    </div>

                    <div class="row" style="padding-top: 5px;">
                        <?php
                        foreach ($rolesList as $grupo => $roles) {
                            ?>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="card">
                                    <div class="card-header contenedor-modulo"><?= $grupo; ?></div>
                                    <div class="card-body" style="padding-left: 30px;">
                                        <!--<label>
                                            <input type="checkbox" class="form-check-input check-todos-modulo" data-grupo="<?= $grupo ?>">
                                            <b>Seleccionar todos para este módulo</b>
                                        </label>
                                        <hr>-->
                                        <?php
                                        foreach ($roles as $rol) {
                                        	$idGrupo=$rol['grupo_id'];
                                        	$idRol=$rol['rol_id'];
                                            $key=$idRol;
                                            $checked=isset($asignadas[$key]) ? 'checked' : '';
                                            ?>
                                            <label for="<?= $key ?>" class="form-check-label ">
                                                <input type="checkbox" id="<?= $key ?>" name="roles[<?= $idRol ?>]" class="form-check-input roles <?= $grupo ?>" <?= $checked ?>>
                                                <?= $rol['rol_nombre'] ?>
                                            </label>
                                            <br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
					<?= $this->Form->button("Asignar",['id'=>'btn-asignar','class'=>'btn btn-success']); ?>
				</div>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("document").ready(function(){

		$("#modal-asignar-roles").modal('show');

		$("#txt-buscar").keyup(function(){
            $(".contenedor-modulo").parent().parent().removeClass('hide');
            busqueda=$("#txt-buscar").val().toLowerCase();
            for(i = 0; i < $(".card").length; i++){
                elemento=$($(".card")[i]);
                card=elemento.find(":Contains("+busqueda+")");
                if(card.length == 0){
                	elemento.parent().addClass('hide');
                }
            }
        });


        $("#btn-asignar").click(function(){
            Swal.fire({
                title: '',
                text: "¿Seguro desea asignar estos roles?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#form-asignar-roles").serialize();
                    $.ajax({
                        type: "POST",
                        url: "<?= $this->Url->build(["action" => "asignarRoles",$user->id]); ?>",
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''});
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
            return false;
        });


	});

</script>