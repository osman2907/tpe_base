<div class="table-responsive">
    <table id="tabla-listado" class="table table-data2 table-hover">
        <thead>
            <tr>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('Personas.cedula','Cédula') ?></th>
                <th scope="col" class="sorter" style="white-space: nowrap;"><?= $this->Paginator->sort('Personas.primer_nombre','Nombre y apellido') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('usuario') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('role','Tipo') ?></th>
                <th scope="col" class="sorter">Eliminado</th>
                <th scope="col" class="actions encabezado-fijo d-none d-sm-none d-md-block"><?= __('Acciones') ?></th>
                <th scope="col" class="encabezado-fijo d-block d-sm-block d-md-none">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id); ?></td>
                <td style="text-align:right; white-space: nowrap;"><?= strtoupper($user->persona->prefijo)."-".$user->persona->cedula; ?></td>
                <td style="white-space: nowrap;"><?= $user->persona->primer_nombre." ".$user->persona->primer_apellido; ?></td>
                <td><?= $user->usuario; ?></td>
                <td><?= ucwords($user->role); ?></td>
                <td>
                    <?php
                    $clase=$user->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                    ?>
                    <h4><span class="<?= $clase ?>"><?= $user->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                </td>

                <!--Acciones para equipos de escritorio-->
                <td class="actions columna-fija d-none d-sm-none d-md-block" style="white-space: nowrap;">
                    <?php if($user->status_id == 101){ ?>

                        <?php if(isset($permisos['app']['users']['consultar'])) {?>
                            <button type="button" class="btn btn-info btn-sm btn-consultar" data-url="<?= $this->Url->build(["action" => "consultar", $user->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Consultar" data-original-title="Consultar"><i class="fa fa-eye"></i></button>
                        <?php } ?>

                        <?php if(isset($permisos['app']['users']['editar'])) {?>
                            <button type="button" class="btn btn-primary btn-sm btn-editar" data-url="<?= $this->Url->build(["action" => "editar", $user->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                        <?php } ?>

                        <?php if(isset($permisos['app']['users']['editarcontrasena'])) {?>
                            <button type="button" class="btn btn-warning btn-sm btn-contrasena" data-url="<?= $this->Url->build(["action" => "editarContrasena", $user->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar contraseña" data-original-title="Editar contraseña"><i class="fa fa-key"></i></button>
                        <?php } ?>
                        
                        <?php if($user->role != 'administrador' && isset($permisos['app']['users']['asignarroles'])){ ?>
                            <button type="button" class="btn btn-success btn-sm btn-roles" data-url="<?= $this->Url->build(["action" => "asignarRoles", $user->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Asignar roles" data-original-title="Asignar roles"><i class="fa fa-tags"></i></button>
                        <?php } ?>
                        
                        <?php if(isset($permisos['app']['users']['eliminar'])) {?>
                            <button type="button" class="btn btn-danger btn-sm btn-eliminar" data-toggle="tooltip" data-placement="bottom" title="Eliminar" data-original-title="Eliminar" data-url="<?= $this->Url->build(["action" => "eliminar", $user->id]); ?>"><i class="fa fa-trash"></i></button>
                        <?php } ?>
                    <?php } ?>

                    <?php if($user->status_id == 102 && isset($permisos['app']['users']['reciclar'])){ ?>
                        <button type="button" class="btn btn-success btn-sm btn-reciclar" data-toggle="tooltip" data-placement="bottom" title="Reciclar" data-original-title="Reciclar" data-url="<?= $this->Url->build(["action" => "reciclar", $user->id]); ?>"><i class="fa fa-recycle"></i></button>
                    <?php } ?>
                </td>
                <!--Fin acciones para equipos de escritorio-->


                <!--Acciones para equipos móviles-->
                <td class="columna-fija d-block d-sm-block d-md-none">
                    <div class="btn-group dropleft">
                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                        </button>
                        <div class="dropdown-menu">
                            <?php if($user->status_id == 101){ ?>
                                <?php if(isset($permisos['app']['users']['consultar'])) {?>
                                    <a class="dropdown-item btn-consultar" data-url="<?= $this->Url->build(["action" => "consultar", $user->id]); ?>">Consultar</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['users']['editar'])) {?>
                                    <a class="dropdown-item btn-editar" data-url="<?= $this->Url->build(["action" => "editar", $user->id]); ?>">Editar</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['users']['editarcontrasena'])) {?>
                                    <a class="dropdown-item btn-contrasena" data-url="<?= $this->Url->build(["action" => "editarContrasena", $user->id]); ?>">Editar contraseña</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['users']['asignarroles'])) {?>
                                    <a class="dropdown-item btn-roles" data-url="<?= $this->Url->build(["action" => "asignarRoles", $user->id]); ?>">Asignar roles</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['users']['eliminar'])) {?>
                                    <a class="dropdown-item btn-eliminar" data-url="<?= $this->Url->build(["action" => "eliminar", $user->id]); ?>">Eliminar</a>
                                <?php } ?>
                            <?php } ?>

                            <?php if($user->status_id == 102){ ?>
                                <?php if(isset($permisos['app']['users']['reciclar'])) {?>
                                    <a class="dropdown-item btn-reciclar" data-url="<?= $this->Url->build(["action" => "reciclar", $user->id]); ?>">Reciclar</a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </td>
                <!--Fin acciones para equipos móviles-->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Página <b>{{page}}</b> de <b>{{pages}}</b>, mostrando <b>{{current}}</b> registros de <b>{{count}}</b> en total')]) ?></p>
</div>

<script type="text/javascript">
    $(".sorter a, .pagination a").click(function() {
        if (!$(this).attr('href')){
            return false;
        }

        href=$(this).attr('href');
        
        $.ajax({
            type: "POST",
            url: href,
            dataType: "html",
            data:$("#listado-users").serialize(),
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#container-listado").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
               $(".page-loader").addClass('hide');
               Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });

        return false;
    });


    $(".btn-consultar").click(function(){
        url=$(this).data('url');
        data=$("#listado-users").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-editar").click(function(){
        url=$(this).data('url');
        data=$("#listado-users").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-contrasena").click(function(){
        url=$(this).data('url');
        data=$("#listado-users").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-roles").click(function(){
        url=$(this).data('url');
        data=$("#listado-users").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-eliminar").click(function(){
        url=$(this).data('url');

        Swal.fire({
            title: '',
            text: "¿Seguro desea eliminar este usuario?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value){
                data=$("#listado-users-roles").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data:data,
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        $(".page-loader").removeClass('hide');
                    },
                    success: function(data){
                        $(".page-loader").addClass('hide');
                        if(data.result){
                            Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                            .then((result) => {
                                if(result.value){
                                    selPage=$(".page-number[tabindex='-1']");
                                    if(selPage.length === 0){
                                        $("#buscar").trigger('click');
                                    }else{
                                        selPage.trigger('click');
                                    }
                                }
                            });
                        }else{
                            Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                        }
                    },
                    error: function (status, error, datos){
                        $(".page-loader").addClass('hide');
                        Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                    }
                });
            }
        });
    });


    $(".btn-reciclar").click(function(){
        url=$(this).data('url');

        Swal.fire({
            title: '',
            text: "¿Seguro desea reciclar este usuario?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Reciclar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value){
                data=$("#listado-users").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data:data,
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        $(".page-loader").removeClass('hide');
                    },
                    success: function(data){
                        $(".page-loader").addClass('hide');
                        if(data.result){
                            Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                            .then((result) => {
                                if(result.value){
                                    selPage=$(".page-number[tabindex='-1']");
                                    if(selPage.length === 0){
                                        $("#buscar").trigger('click');
                                    }else{
                                        selPage.trigger('click');
                                    }
                                }
                            });
                        }else{
                            Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                        }
                    },
                    error: function (status, error, datos){
                        $(".page-loader").addClass('hide');
                        Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                    }
                });
            }
        });
    });

</script>