<div class="modal fade" id="modal-consultar-user" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Consultar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-consulta1">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="titulo">Cédula</div>
                            <div class="contenido"><?= strtoupper($user->persona->prefijo)."-".$user->persona->cedula; ?></div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="titulo">Nombres</div>
                            <div class="contenido"><?= $user->persona->primer_nombre." ".$user->persona->segundo_nombre; ?></div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="titulo">Apellidos</div>
                            <div class="contenido"><?= $user->persona->primer_apellido." ".$user->persona->segundo_apellido; ?></div>
                        </div>
                    
                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="titulo">Usuario</div>
                            <div class="contenido"><?= $user->usuario; ?></div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="titulo">Tipo</div>
                            <div class="contenido"><?= ucwords($user->role); ?></div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="titulo">Eliminado</div>
                            <div class="contenido">
                                <?php
                                $clase=$user->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                                ?>
                                <h4><span class="<?= $clase ?>"><?= $user->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                            </div>
                        </div>
                    </div>

                    <div class="row separacion">
                        <div class="col-12">
                            <div class="titulo">Roles asociados a este usuario</div>
                            <div class="contenido">
                                <div class="row">
                                    <?php 
                                    foreach($roles as $rol):
                                        $nombre=$rol->users_role->nombre;
                                        ?>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                            <i class="fa fa-check"></i>
                                            <?= $nombre ?>
                                        </div>
                                        <?php
                                    endforeach;

                                    if(count($roles) == 0){
                                        ?>
                                        <div class="alert alert-warning ancho-completo">
                                            Este usuario no ha sido asociado a ningún rol
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br><br>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-consultar-user").modal('show');
    });
</script>
