<?= $this->Form->create($user,['id'=>'form-nuevo-usuario2','autocomplete'=>'off']) ?>
	
	<?php echo $this->Form->hidden('accion',['value'=>'registrar_usuario']);   ?>
	<?php echo $this->Form->hidden('persona_id',['id'=>'txt-persona-id']);   ?>
	
	<div class="row">
		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('cedula',['class'=>'form-control','readonly'=>'readonly','label'=>'Cédula']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('primer_nombre',['class'=>'form-control','readonly'=>'readonly']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('primer_apellido',['class'=>'form-control','readonly'=>'readonly']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('usuario',['class'=>'form-control']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('contrasena',['type'=>'password','class'=>'form-control','autocomplete'=>'new-password','label'=>'Contraseña']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('role',['options'=>$roleList, 'class'=>'form-control','label'=>'Tipo']); ?>
		</div>
	</div>

<?= $this->Form->end() ?>
		

<script type="text/javascript">
	$("document").ready(function(){

		$("#btn-registrar").click(function(){
	  		var data=$("#form-nuevo-usuario2").serialize();
	  		$.ajax({
	            type: "POST",
	            url: "<?= $this->Url->build(["action" => "registrar"]); ?>",
	            data:data,
	            dataType: "json",
	            beforeSend: function(xhr){
	                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
	                $(".page-loader").removeClass('hide');
	            },
	            success: function(data){
	            	$(".page-loader").addClass('hide');
	            	$("label[class='error']").remove();
	            	if(data.errors){
	            		errores=data.errors;
	            		$.each(errores, function(i,mensaje){
	            			$("[name='"+i+"']").parent().append('<label class="error">'+mensaje+'</label>');
	            		});
	            		return false;
	            	}

	            	$(".close").trigger("click");
	            	if(data.result){
	            		Swal.fire({
				            icon: 'success',
				            title: 'Registro exitoso',
				            text: '',
				            footer: ''
				        }).then((result) => {
				        	if(result.value){
				        		selPage=$(".page-number[tabindex='-1']");
				        		if(selPage.length === 0){
				        			$("#buscar").trigger('click');
				        		}else{
				        			selPage.trigger('click');
				        		}
				        	}
				        });
	            	}else{
	            		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: 'Error al registrar, por favor intente nuevamente',
				            footer: ''
				        });
	            	}
	            },
	            error: function (status, error, datos){
	            	$(".page-loader").addClass('hide');
	            	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al registrar, por favor intente nuevamente',
			            footer: ''
			        });
	            }
	        });

	        return false;
	    });


	    $("#form-nuevo-usuario2").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				$("#btn-registrar").trigger('click');
			}
		});


        $("#btn-anterior").click(function(){
        	idPersona=$("#txt-persona-id").val();
			$.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["action" => "registrar"]); ?>",
                data:{
                	accion:'formulario_personas',
                	persona_id:idPersona
                },
                dataType: "html",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                	$(".page-loader").addClass('hide');
	            	$(".modal-body").html(data);
	            	$( "#btn-anterior,#btn-registrar").unbind();
	            	$("#btn-siguiente").removeClass('hide');
	            	$("#btn-registrar").addClass('hide');
                },
                error: function (status, error, datos){
                	$(".page-loader").addClass('hide');
                	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al registrar, por favor intente nuevamente',
			            footer: ''
			        });
                }
            });
			return false;
		});

	});

</script>