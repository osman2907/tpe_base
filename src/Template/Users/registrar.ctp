<div class="modal fade" id="modal-nuevo-usuario" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<?= $this->Form->create($user,['id'=>'form-nuevo-usuario']) ?>
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Nuevo usuario</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body contenedor-formulario">
					<?= $this->Form->hidden('accion',['value'=>'registrar']);   ?>
				</div>
				<div class="modal-footer">
					<?= $this->Form->button("Anterior",['id'=>'btn-anterior','class'=>'btn btn-secondary']); ?>
					<?= $this->Form->button("Siguiente",['id'=>'btn-siguiente','class'=>'btn btn-primary']); ?>
					<?= $this->Form->button("Registrar",['id'=>'btn-registrar','class'=>'btn btn-success']); ?>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	$("document").ready(function(){

		$("#modal-nuevo-usuario").modal('show');
  		var data=$("#form-nuevo-usuario").serialize();
  		$.ajax({
            type: "POST",
            url: "<?= $this->Url->build(["action" => "registrar"]); ?>",
            data:data,
            dataType: "html",
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
            	$(".page-loader").addClass('hide');
            	$(".modal-body").html(data);
            	$("#btn-anterior").addClass('hide');
            	$("#btn-registrar").addClass('hide');
            },
            error: function (status, error, datos){
            	$(".page-loader").addClass('hide');
            	Swal.fire({
		            icon: 'error',
		            title: '',
		            text: 'Error al registrar, por favor intente nuevamente',
		            footer: ''
		        });
            }
        });


        /*$('#modal-nuevo-usuario').on('hidden.bs.modal', function () {
			alert("cerrando");
		});*/

	});

</script>