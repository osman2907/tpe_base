<div class="modal fade" id="modal-consultar-rol" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Consultar rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="container-consulta1">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div class="titulo">Nombre</div>
                        <div class="contenido"><?= $userRole->nombre; ?></div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div class="titulo">Grupo</div>
                        <div class="contenido"><?= $userRole->users_group->nombre; ?></div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div class="titulo">Eliminado</div>
                        <div class="contenido">
                            <?php
                            $clase=$userRole->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                            ?>
                            <h4><span class="<?= $clase ?>"><?= $userRole->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                        </div>
                    </div>
                </div>

                <div class="row separacion">
                    <div class="col-12">
                        <div class="titulo">Rutas asignadas a este rol</div>
                        <div class="contenido">
                            <div class="row">
                                <?php
                                foreach ($asignadas as $fila) {
                                    $plugin=$fila['plugin'];
                                    $modulo=$fila['plugin'] == 'App' ? 'Sistema' : $fila['plugin'];
                                    $controller=$fila['controller'];
                                    $grupo="$plugin-$controller";
                                    ?>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="card">
                                            <div class="card-header contenedor-modulo"><?= $modulo." / ".$controller; ?></div>
                                            <div class="card-body">
                                                <?php
                                                foreach ($fila['acciones'] as $accion) {
                                                    ?><i class="fa fa-check"></i>
                                                    <?php
                                                    echo $accion['action']."<br>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }

                                if(count($asignadas) == 0){
                                    ?>
                                    <div class="alert alert-warning ancho-completo">
                                        Este rol no tiene rutas asociadas
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-consultar-rol").modal('show');
    });
</script>
