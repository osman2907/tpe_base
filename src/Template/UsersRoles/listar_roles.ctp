<div class="table-responsive table-responsive-data2">
    <table id="tabla-listado" class="table table-data2 table-hover">
        <thead>
            <tr>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('UsersGroups.nombre','Grupo') ?></th>
                <th scope="col" class="sorter">Eliminado</th>
                <th scope="col" class="actions encabezado-fijo"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersRoles as $rol): ?>
            <tr>
                <td><?= $this->Number->format($rol->id); ?></td>
                <td style="white-space: nowrap;"><?= $rol->nombre; ?></td>
                <td style="white-space: nowrap;"><?= $rol->users_group->nombre ?></td>
                <td>
                    <?php
                    $clase=$rol->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                    ?>
                    <h4><span class="<?= $clase ?>"><?= $rol->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                </td>

                <!--Acciones para equipos de escritorio-->
                <td class="actions columna-fija d-none d-sm-none d-md-block" style="white-space: nowrap;">
                    <?php if($rol->status_id == 101){ ?>
                        <?php if(isset($permisos['app']['usersroles']['consultar'])) { ?>
                            <button type="button" class="btn btn-info btn-sm btn-consultar" data-url="<?= $this->Url->build(["action" => "consultar", $rol->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Consultar" data-original-title="Consultar"><i class="fa fa-eye"></i></button>
                        <?php }?>

                        <?php if(isset($permisos['app']['usersroles']['editar'])) { ?>
                            <button type="button" class="btn btn-primary btn-sm btn-editar" data-url="<?= $this->Url->build(["action" => "editar", $rol->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                        <?php }?>
                        
                        <?php if(isset($permisos['app']['usersroles']['editarpermisos'])) { ?>
                            <button type="button" class="btn btn-warning btn-sm btn-permisos" data-url="<?= $this->Url->build(["action" => "editarPermisos", $rol->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar permisos" data-original-title="Editar permisos"><i class="fa fa-lock"></i></button>
                        <?php }?>
                        
                        <?php if(isset($permisos['app']['usersroles']['eliminar'])) { ?>
                            <button type="button" class="btn btn-danger btn-sm btn-eliminar" data-toggle="tooltip" data-placement="bottom" title="Eliminar" data-original-title="Eliminar" data-url="<?= $this->Url->build(["action" => "eliminar", $rol->id]); ?>"><i class="fa fa-trash"></i></button>
                        <?php }?>
                    <?php } ?>

                    <?php if($rol->status_id == 102){ ?>
                        <?php if(isset($permisos['app']['usersroles']['reciclar'])) { ?>
                            <button type="button" class="btn btn-success btn-sm btn-reciclar" data-toggle="tooltip" data-placement="bottom" title="Reciclar" data-original-title="Reciclar" data-url="<?= $this->Url->build(["action" => "reciclar", $rol->id]); ?>"><i class="fa fa-recycle"></i></button>
                        <?php } ?>
                    <?php } ?>
                </td>
                <!--Fin acciones para equipos de escritorio-->

                <!--Acciones para equipos móviles-->
                <td class="columna-fija d-block d-sm-block d-md-none">
                    <div class="btn-group dropleft">
                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                        </button>
                        <div class="dropdown-menu">
                            <?php if($rol->status_id == 101){ ?>
                                <?php if(isset($permisos['app']['usersroles']['consultar'])) {?>
                                    <a class="dropdown-item btn-consultar" data-url="<?= $this->Url->build(["action" => "consultar", $rol->id]); ?>">Consultar</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['usersroles']['editar'])) {?>
                                    <a class="dropdown-item btn-editar" data-url="<?= $this->Url->build(["action" => "editar", $rol->id]); ?>">Editar</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['usersroles']['editarpermisos'])) {?>
                                    <a class="dropdown-item btn-permisos" data-url="<?= $this->Url->build(["action" => "editarPermisos", $rol->id]); ?>">Editar permisos</a>
                                <?php } ?>

                                <?php if(isset($permisos['app']['usersroles']['eliminar'])) {?>
                                    <a class="dropdown-item btn-eliminar" data-url="<?= $this->Url->build(["action" => "eliminar", $rol->id]); ?>">Eliminar</a>
                                <?php } ?>
                            <?php } ?>

                            <?php if($rol->status_id == 102){ ?>
                                <?php if(isset($permisos['app']['usersroles']['reciclar'])) {?>
                                    <a class="dropdown-item btn-reciclar" data-url="<?= $this->Url->build(["action" => "reciclar", $rol->id]); ?>">Reciclar</a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </td>
                <!--Fin acciones para equipos móviles-->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Página <b>{{page}}</b> de <b>{{pages}}</b>, mostrando <b>{{current}}</b> registros de <b>{{count}}</b> en total')]) ?></p>
</div>

<script type="text/javascript">
    $(".sorter a, .pagination a").click(function() {
        if (!$(this).attr('href')){
            return false;
        }

        href=$(this).attr('href');
        
        $.ajax({
            type: "POST",
            url: href,
            dataType: "html",
            data:$("#listado-users-roles").serialize(),
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#container-listado").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
               $(".page-loader").addClass('hide');
               Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });

        return false;
    });


    $(".btn-consultar").click(function(){
        url=$(this).data('url');
        data=$("#listado-users-roles").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-editar").click(function(){
        url=$(this).data('url');
        data=$("#listado-users-roles").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-permisos").click(function(){
        url=$(this).data('url');
        data=$("#listado-users-roles").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-eliminar").click(function(){
        url=$(this).data('url');

        Swal.fire({
            title: '',
            text: "¿Seguro desea eliminar este rol?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value){
                data=$("#listado-users-roles").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data:data,
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        $(".page-loader").removeClass('hide');
                    },
                    success: function(data){
                        $(".page-loader").addClass('hide');
                        if(data.result){
                            Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                            .then((result) => {
                                if(result.value){
                                    selPage=$(".page-number[tabindex='-1']");
                                    if(selPage.length === 0){
                                        $("#buscar").trigger('click');
                                    }else{
                                        selPage.trigger('click');
                                    }
                                }
                            });
                        }else{
                            Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                        }
                    },
                    error: function (status, error, datos){
                        $(".page-loader").addClass('hide');
                        Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                    }
                });
            }
        });
    });


    $(".btn-reciclar").click(function(){
        url=$(this).data('url');

        Swal.fire({
            title: '',
            text: "¿Seguro desea reciclar este rol?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Reciclar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value){
                data=$("#listado-users-roles").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data:data,
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        $(".page-loader").removeClass('hide');
                    },
                    success: function(data){
                        $(".page-loader").addClass('hide');
                        if(data.result){
                            Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                            .then((result) => {
                                if(result.value){
                                    selPage=$(".page-number[tabindex='-1']");
                                    if(selPage.length === 0){
                                        $("#buscar").trigger('click');
                                    }else{
                                        selPage.trigger('click');
                                    }
                                }
                            });
                        }else{
                            Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                        }
                    },
                    error: function (status, error, datos){
                        $(".page-loader").addClass('hide');
                        Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                    }
                });
            }
        });
    });

</script>