<div class="users-groups index large-9 medium-8 columns content">
    <?= $this->Form->create($usersRoles,['id'=>'listado-users-roles']) ?>
        <h3><?= __('Roles') ?></h3>
        <br>
        <?php if(isset($permisos['app']['usersroles']['registrar'])) { ?>
            <?= $this->Html->link(__('Nuevo rol'), ['action' => 'add'],['id'=>'btn-nuevo-rol', 'class'=>'btn btn-success']) ?>
        <?php } ?>
        <div id="container-filtros">
            <div class="row filtros-encabezado">
                <!-- Mostrar en resolución de escritorio-->
                <div class="col-sm-12 d-none d-sm-none d-md-block">
                    Filtros listado de roles
                    <div class="float-right">
                        <button type="button" class="btn btn-danger btn-sm btn-ocultar-filtros hide" data-toggle="tooltip" data-placement="bottom" title="Ocultar filtros" data-original-title="Ocultar filtros"><i class="fa fa-filter"></i> Ocultar Filtros</button>

                        <button type="button" class="btn btn-info btn-sm btn-mostrar-filtros" data-toggle="tooltip" data-placement="bottom" title="Mostrar filtros" data-original-title="Mostrar filtros"><i class="fa fa-filter"></i> Mostrar Filtros</button>
                    </div>
                </div>

                <!-- Mostrar en resolución móvil-->
                <div class="col-sm-12 d-block d-sm-block d-md-none filtros-encabezado">
                    Filtros
                    <div class="float-right">
                        <button type="button" class="btn btn-danger btn-sm btn-ocultar-filtros hide" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-filter"></i> Ocultar</button>

                        <button type="button" class="btn btn-info btn-sm btn-mostrar-filtros" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-filter"></i> Mostrar</button>
                    </div>
                </div>
            </div>

            <div class="row filtros-cuerpo hide">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    echo $this->Form->label("nombre","Nombre",['style'=>'display: block']); 
                    echo $this->Form->text('filtros[nombre]',['class'=>'form-control-sm','style'=>'width:100%']); 
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    echo $this->Form->label("users_group_id","Grupo",['style'=>'display: block']); 
                    echo $this->Form->select('filtros[users_group_id]',$gruposList,['class'=>'form-control-sm','empty'=>'Seleccione grupo','style'=>'width:100%']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    $mostrarList=[101=>'Registros activos',102=>'Registros eliminados'];
                    echo $this->Form->label("mostrar","Mostrar",['style'=>'display: block']); 
                    echo $this->Form->select('filtros[mostrar]',$mostrarList,['class'=>'form-control-sm','style'=>'width:100%','empty'=>'Todos','value'=>101]); 
                    ?>
                </div>
            </div>

            <div class="row filtros-pie hide">
                <div class="col-6 col-md-4 col-lg-2">
                    <button id="buscar" type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-search"></i>&nbsp; Buscar
                    </button>
                </div>

                <div class="col-6 col-md-4 col-lg-2">
                    <button id="limpiar" type="reset" class="btn btn-warning btn-sm">
                        <i class="fa fa-eraser"></i>&nbsp; Limpiar
                    </button>
                </div>

                <div class="col-12 d-block d-sm-block d-md-none">
                    <button id="ocultar" type="button" class="btn btn-danger btn-sm btn-ocultar-filtros">
                        <i class="fa fa-filter"></i>&nbsp; Ocultar filtros
                    </button>
                </div>
            </div>

        </div>

        <br>

        <div id="container-listado"></div>
        
    <?= $this->Form->end() ?>

</div>


<script type="text/javascript">
    $(document).ready(function(){

        function cargar_tabla(){
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "usersRoles", "action" => "listar"]); ?>",
                dataType: "html",
                data:$("#listado-users-roles").serialize(),
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#container-listado").html(data);
                    $('html,body').animate({ scrollTop: $("#tabla-listado").offset().top }, 1000);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        }
        cargar_tabla();


        $("#btn-nuevo-rol").click(function(){
            data=$("#listado-users-roles").serialize();
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "usersRoles", "action" => "registrar"]); ?>",
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        });


        $("#buscar").click(function(){
            cargar_tabla();
            return false;
        });


    });
</script>
