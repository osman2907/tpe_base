<div class="modal fade" id="modal-editar-permisos-rol" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Editar permisos rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create($userRole,['id'=>'form-editar-permisos-rol']) ?>
                    <?= $this->Form->control('id'); ?>
                    <div class="container-consulta1">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Nombre</div>
                                <div class="contenido"><?= $userRole->nombre; ?></div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Grupo</div>
                                <div class="contenido"><?= $userRole->users_group->nombre; ?></div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="titulo">Eliminado</div>
                                <div class="contenido">
                                    <?php
                                    $clase=$userRole->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                                    ?>
                                    <h4><span class="<?= $clase ?>"><?= $userRole->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row separacion">
                        <div class="col-lg-12">
                            <div class="alert-warning" style="padding: 5px;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label><b>Buscar módulos:</b></label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" id="txt-buscar" class="form-control" placeholder="Buscar...">
                                    </div>
                                    <div class="col-sm-3 offset-sm-1">
                                        <label>
                                            <input type="checkbox" id="check-todos" class="form-check-input">
                                            <b>Seleccionar todos</b>
                                        </label>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12" style="text-align: center; padding-top: 5px;">
                                    <input type="button" class="btn btn-success btn-guardar-permisos" value="Guardar permisos">
                                </div>
                            </div>

                            <div class="row" style="padding-top: 5px;">
                                
                                <?php
                                $idRol=$userRole->id;
                                foreach ($rutas as $fila) {
                                    $plugin=$fila['plugin'];
                                    $modulo=$fila['plugin'] == 'App' ? 'Sistema' : $fila['plugin'];
                                    $controller=$fila['controller'];
                                    $grupo="$plugin-$controller";
                                    ?>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="card">
                                            <div class="card-header contenedor-modulo"><?= $modulo." / ".$controller; ?></div>
                                            <div class="card-body" style="padding-left: 30px;">
                                                <label>
                                                    <input type="checkbox" class="form-check-input check-todos-modulo" data-grupo="<?= $grupo ?>">
                                                    <b>Seleccionar todos para este módulo</b>
                                                </label>
                                                <hr>
                                                <?php
                                                foreach ($fila['acciones'] as $key => $accion) {
                                                    $key="$idRol~$plugin~$controller~$accion";
                                                    $checked=isset($asignadas[$key]) ? 'checked' : '';
                                                    ?>
                                                    <label for="<?= $key ?>" class="form-check-label ">
                                                        <input type="checkbox" id="<?= $key ?>" name="rutas[<?= $plugin ?>][<?= $controller ?>][<?= $accion ?>]" class="form-check-input rutas <?= $grupo ?>" <?= $checked ?>>
                                                        <?= $accion ?>
                                                    </label>
                                                    <br>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <div class="row">
                                <div class="col-sm-12" style="text-align: center; padding-top: 5px;">
                                    <input type="button" class="btn btn-success btn-guardar-permisos" value="Guardar permisos">
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php //debug($asignadas); ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-editar-permisos-rol").modal('show');

        $("#txt-buscar").keyup(function(){
            $(".contenedor-modulo").parent().parent().removeClass('hide');
            busqueda=$("#txt-buscar").val().toLowerCase();
            for(i = 0; i < $(".contenedor-modulo").length; i++){
                elemento=$($(".contenedor-modulo")[i]);
                modulo=elemento.text().toLowerCase();
                if(modulo.indexOf(busqueda) == -1){
                    elemento.parent().parent().addClass('hide');
                }
            }
        });

        $("#check-todos").change(function(){
            check=$("#check-todos").prop('checked');
            $("input[type='checkbox'].rutas").each(function(){
                if(check){
                    $(this).prop("checked", true);
                }else{
                    $(this).prop("checked", false);
                }
            });

            $("input[type='checkbox'].check-todos-modulo").each(function(){
                if(check){
                    $(this).prop("checked", true);
                }else{
                    $(this).prop("checked", false);
                }
            });
        });

        $(".check-todos-modulo").change(function(){
            check=$(this).prop('checked');
            grupo=$(this).data('grupo');
            $("input[type='checkbox'].rutas."+grupo).each(function(){
                if(check){
                    $(this).prop("checked", true);
                }else{
                    $(this).prop("checked", false);
                }
            });
        });

        $(".check-todos-modulo").each(function(){
            grupo=$(this).data('grupo');
            $(this).prop("checked",true);
            $("."+grupo).each(function(){
                if($(this).prop('checked') == false){
                    $(".check-todos-modulo[data-grupo='"+grupo+"']").prop('checked',false);
                }
            })
        });

        $("input[type='checkbox'].rutas").change(function(){
            labelGrupo=$(this).parent().parent().children('label')[0];
            grupo=$(labelGrupo).children("input[type='checkbox']").data('grupo');
            $(labelGrupo).children("input[type='checkbox']").prop("checked",true);
            $("."+grupo).each(function(){
                if($(this).prop('checked') == false){
                    $(".check-todos-modulo[data-grupo='"+grupo+"']").prop('checked',false);
                }
            })
        });


        $(".btn-guardar-permisos").click(function(){
            Swal.fire({
                title: '',
                text: "¿Seguro desea guardar estos permisos?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Guardar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#form-editar-permisos-rol").serialize();
                    $.ajax({
                        type: "POST",
                        url: "<?= $this->Url->build(["action" => "editarPermisos"]); ?>",
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''});
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
        });
        

    });
</script>
