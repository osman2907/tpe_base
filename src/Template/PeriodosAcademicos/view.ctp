<div class="modal fade" id="modal-consultar-periodo-academico" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Consultar período académico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-bordered">
                                <tr class="table-active">
                                    <th>Nombre</th>
                                    <th>Fecha inicio</th>
                                    <th>Fecha fin</th>
                                </tr>

                                <tr>
                                    <td><?= $periodosAcademico->nombre; ?></td>
                                    <td><?= date_format($periodosAcademico->fecha_inicio, 'd/m/Y') ?></td>
                                    <td><?= date_format($periodosAcademico->fecha_fin, 'd/m/Y') ?></td>
                                </tr>

                                <tr class="table-active">
                                    <th colspan="3">Estatus</th>
                                </tr>

                                <tr>
                                    <td colspan="3"><?= $periodosAcademico->estatus->nombre; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-consultar-periodo-academico").modal('show');
    });
</script>
