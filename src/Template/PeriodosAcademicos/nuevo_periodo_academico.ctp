<div class="modal fade" id="modal-nuevo-periodo-academico" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<?= $this->Form->create($periodosAcademicos,['id'=>'form-nuevo-periodo-academico']) ?>
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Nuevo período académico</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
						<div class="row">
	              			<div class="col-lg-4">
	              				<?= $this->Form->control('nombre',['class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('fecha_inicio',['id'=>'fecha_inicio','type'=>'text','class'=>'form-control datepicker','autocomplete'=>'off']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('fecha_fin',['id'=>'fecha_fin','type'=>'text','class'=>'form-control datepicker','autocomplete'=>'off']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('estatus_id',['options'=>$estatusList, 'class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('status_id', ['type' => 'hidden', 'class'=>'form-control']); ?>
	                		</div>

	                	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<?= $this->Form->button("Registrar",['id'=>'btn-registrar','class'=>'btn btn-success']); ?>
				</div>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("document").ready(function(){

		$("#modal-nuevo-periodo-academico").modal('show');


		$('#fecha_inicio').datepicker({
			onSelect : function(selectedDate){
				$('#fecha_fin').datepicker('option', 'minDate', selectedDate);
			}
		});


		$('#fecha_fin').datepicker({
			onSelect : function(selectedDate){
				$('#fecha_inicio').datepicker('option', 'maxDate', selectedDate);
			}
		});


  		$("#btn-registrar").click(function(){
  			data=$("#form-nuevo-periodo-academico").serialize();
  			$.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["action" => "crearPeriodoAcademico"]); ?>",
                data:data,
                dataType: "json",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                	$(".page-loader").addClass('hide');
                	if(data.errors){
                		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: 'Por favor cumplir con todas las reglas de validación',
				            footer: ''
				        });
                		return false;
                	}

                	$(".close").trigger("click");
                	if(data.result){
                		Swal.fire({
				            icon: 'success',
				            title: 'Registro exitoso',
				            text: '',
				            footer: ''
				        }).then((result) => {
				        	if(result.value){
				        		$(".page-loader").removeClass('hide');
				        		location.reload();
				        	}
				        });
                	}else{
                		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: 'Error al registrar, por favor intente nuevamente',
				            footer: ''
				        });
                	}
                },
                error: function (status, error, datos){
                	$(".page-loader").addClass('hide');
                	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al registrar, por favor intente nuevamente',
			            footer: ''
			        });
                }
            });
  			return false;
  		});

	});
</script>