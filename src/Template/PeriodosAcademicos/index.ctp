<div class="periodosAcademicos index large-9 medium-8 columns content">
    <?= $this->Form->create($periodosAcademicos,['id'=>'listado-periodos-academicos']) ?>
        <h3><?= __('Periodos Academicos') ?></h3>
        <br>
        <?= $this->Html->link(__('Nuevo período académico'), ['action' => 'add'],['id'=>'btn-nuevo-periodo-academico', 'class'=>'btn btn-success']) ?>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2 table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('fecha_inicio') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('fecha_fin') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('estatus_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status_id','Eliminado') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($periodosAcademicos as $periodosAcademico): ?>
                    <tr>
                        <td><?= $this->Number->format($periodosAcademico->id) ?></td>
                        <td><?= h($periodosAcademico->nombre) ?></td>
                        <td><?= date_format($periodosAcademico->fecha_inicio, 'd/m/Y') ?></td>
                        <td><?= date_format($periodosAcademico->fecha_fin, 'd/m/Y') ?></td>
                        <td>
                            <?php
                            $clase=$periodosAcademico->estatus_id == 2 ? 'badge badge-success' : 'badge badge-danger';
                            ?>
                            <h4><span class="<?= $clase ?>"><?= $periodosAcademico->estatus->nombre; ?></span></h4>
                        </td>
                         <td>
                            <?php
                            $clase=$periodosAcademico->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                            ?>
                            <h4><span class="<?= $clase ?>"><?= $periodosAcademico->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                        </td>
                        <td class="actions">
                            <?php if($periodosAcademico->status_id == 101){ ?>
                                <button type="button" class="btn btn-info btn-sm btn-consultar" data-url="<?= $this->Url->build(["action" => "view", $periodosAcademico->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Consultar" data-original-title="Consultar"><i class="fa fa-eye"></i></button>
                                <button type="button" class="btn btn-primary btn-sm btn-modificar" data-url="<?= $this->Url->build(["action" => "editarPeriodoAcademico", $periodosAcademico->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                            <?php } ?>
                            
                            <?php if($periodosAcademico->estatus_id == 2 && $periodosAcademico->status_id == 101){ ?>
                                <button type="button" class="btn btn-danger btn-sm btn-desactivar" data-toggle="tooltip" data-placement="bottom" title="Desactivar" data-original-title="Desactivar" data-url="<?= $this->Url->build(["action" => "desactivarPeriodoAcademico", $periodosAcademico->id]); ?>"><i class="fa fa-minus-circle"></i></button>
                            <?php } ?>

                            <?php if($periodosAcademico->estatus_id == 3 && $periodosAcademico->status_id == 101){ ?>
                                <button type="button" class="btn btn-success btn-sm btn-activar" data-toggle="tooltip" data-placement="bottom" title="Activar" data-original-title="Activar" data-url="<?= $this->Url->build(["action" => "activarPeriodoAcademico", $periodosAcademico->id]); ?>"><i class="fa fa-check-circle"></i></button>
                            <?php } ?>

                            <?php if($periodosAcademico->status_id == 101){ ?>
                                <button type="button" class="btn btn-danger btn-sm btn-eliminar" data-toggle="tooltip" data-placement="bottom" title="Eliminar" data-original-title="Eliminar" data-url="<?= $this->Url->build(["action" => "eliminarPeriodoAcademico", $periodosAcademico->id]); ?>"><i class="fa fa-trash"></i></button>
                            <?php } ?>

                            <?php if($periodosAcademico->status_id == 102){ ?>
                                <button type="button" class="btn btn-success btn-sm btn-reciclar" data-toggle="tooltip" data-placement="bottom" title="Reciclar" data-original-title="Reciclar" data-url="<?= $this->Url->build(["action" => "reciclarPeriodoAcademico", $periodosAcademico->id]); ?>"><i class="fa fa-recycle"></i></button>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página <b>{{page}}</b> de <b>{{pages}}</b>, mostrando <b>{{current}}</b> registros de <b>{{count}}</b> en total')]) ?></p>
        </div>
    <?= $this->Form->end() ?>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        $("#btn-nuevo-periodo-academico").click(function(){
            data=$("#listado-periodos-academicos").serialize();
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "PeriodosAcademicos", "action" => "nuevoPeriodoAcademico"]); ?>",
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        });


        $(".btn-consultar").click(function(){
            url=$(this).data('url');
            data=$("#listado-periodos-academicos").serialize();
            $.ajax({
                type: "POST",
                url: url,
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                    $(".page-loader").addClass('hide');
                    Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
        });


        $(".btn-modificar").click(function(){
            url=$(this).data('url');
            data=$("#listado-periodos-academicos").serialize();
            $.ajax({
                type: "POST",
                url: url,
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                    $(".page-loader").addClass('hide');
                    Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
        });


        $(".btn-desactivar").click(function(){
            url=$(this).data('url');

            Swal.fire({
                title: '',
                text: "¿Seguro desea desactivar este período académico?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Desactivar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#listado-periodos-academicos").serialize();
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                                .then((result) => {
                                    if(result.value){
                                        $(".page-loader").removeClass('hide');
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
        });



        $(".btn-activar").click(function(){
            url=$(this).data('url');

            Swal.fire({
                title: '¿Seguro desea activar este período académico?',
                text: "Esta acción desactivará de manera automática el período académico que esté activo en este momento",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Activar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#listado-periodos-academicos").serialize();
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                                .then((result) => {
                                    if(result.value){
                                        $(".page-loader").removeClass('hide');
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                            $(".page-loader").addClass('hide');
                        }
                    });
                }
            });
        });



        $(".btn-eliminar").click(function(){
            url=$(this).data('url');

            Swal.fire({
                title: '',
                text: "¿Seguro desea eliminar este período académico?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#listado-periodos-academicos").serialize();
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                                .then((result) => {
                                    if(result.value){
                                        $(".page-loader").removeClass('hide');
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
        });


        $(".btn-reciclar").click(function(){
            url=$(this).data('url');

            Swal.fire({
                title: '',
                text: "¿Seguro desea reciclar este período académico?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Reciclar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#listado-periodos-academicos").serialize();
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                                .then((result) => {
                                    if(result.value){
                                        $(".page-loader").removeClass('hide');
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
        });

    });
</script>
