<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PeriodosAcademico $periodosAcademico
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Periodos Academicos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Descripcions'), ['controller' => 'Descripcions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Descripcion'), ['controller' => 'Descripcions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="periodosAcademicos form large-9 medium-8 columns content">
    <?= $this->Form->create($periodosAcademico) ?>
    <fieldset>
        <legend><?= __('Add Periodos Academico') ?></legend>
        <?php
            echo $this->Form->control('nombre');
            echo $this->Form->control('fecha_inicio');
            echo $this->Form->control('fecha_fin');
            echo $this->Form->control('estatus_id');
            echo $this->Form->control('status_id', ['options' => $descripcions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
