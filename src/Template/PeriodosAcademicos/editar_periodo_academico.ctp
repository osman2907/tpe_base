<div class="modal fade" id="modal-editar-periodo-academico" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<?= $this->Form->create($periodosAcademicos,['id'=>'form-editar-periodo-academico']) ?>
		<?= $this->Form->control('id'); ?>
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Editar período académico</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
						<div class="row">
	              			<div class="col-lg-4">
	              				<?= $this->Form->control('nombre',['class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('fecha_inicio',['id'=>'fecha_inicio','type'=>'text','class'=>'form-control datepicker',
	              					'value'=>date_format($periodosAcademicos->fecha_inicio, 'd/m/Y'),'autocomplete'=>'off']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('fecha_fin',['id'=>'fecha_fin','type'=>'text','class'=>'form-control datepicker',
	              					'value'=>date_format($periodosAcademicos->fecha_fin, 'd/m/Y'),'autocomplete'=>'off']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('status_id', ['type' => 'hidden', 'class'=>'form-control']); ?>
	                		</div>

	                	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<?= $this->Form->button("Editar",['id'=>'btn-editar','class'=>'btn btn-success']); ?>
				</div>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("document").ready(function(){

		$("#modal-editar-periodo-academico").modal('show');


  		$('#fecha_inicio').datepicker({
			maxDate: $("#fecha_fin").val(),
			onSelect : function(selectedDate){
				$('#fecha_fin').datepicker('option', 'minDate', selectedDate);
			}
		});


		$('#fecha_fin').datepicker({
			minDate: $("#fecha_inicio").val(),
			onSelect : function(selectedDate){
				$('#fecha_inicio').datepicker('option', 'maxDate', selectedDate);
			}
		});

  
		$("#btn-editar").click(function(){
			data=$("#form-editar-periodo-academico").serialize();
			$.ajax({
	            type: "POST",
	            url: "<?= $this->Url->build(["action" => "actualizarPeriodoAcademico"]); ?>",
	            data:data,
	            dataType: "json",
	            beforeSend: function(xhr){
	                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
	                $(".page-loader").removeClass('hide');
	            },
	            success: function(data){
	            	$(".page-loader").addClass('hide');
	            	if(data.errors){
	            		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: 'Por favor cumplir con todas las reglas de validación',
				            footer: ''
				        });
	            		return false;
	            	}
	            	$(".close").trigger("click");

	            	if(data.result){
	            		Swal.fire({
				            icon: 'success',
				            title: data.mensaje,
				            text: '',
				            footer: ''
				        }).then((result) => {
				        	if(result.value){
				        		$(".page-loader").removeClass('hide');
				        		location.reload();
				        	}
				        });
	            	}else{
	            		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: data.mensaje,
				            footer: ''
				        });
	            	}
	            },
	            error: function (status, error, datos){
	            	$(".page-loader").addClass('hide');
	            	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al actualizar, por favor intente nuevamente',
			            footer: ''
			        });
	            }
	        });
			return false;
		});
	});

</script>