<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UsersRolesAssignment $usersRolesAssignment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $usersRolesAssignment->user_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $usersRolesAssignment->user_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users Roles Assignments'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users Roles'), ['controller' => 'UsersRoles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Users Role'), ['controller' => 'UsersRoles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersRolesAssignments form large-9 medium-8 columns content">
    <?= $this->Form->create($usersRolesAssignment) ?>
    <fieldset>
        <legend><?= __('Edit Users Roles Assignment') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
