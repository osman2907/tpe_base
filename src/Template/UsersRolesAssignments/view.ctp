<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UsersRolesAssignment $usersRolesAssignment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Roles Assignment'), ['action' => 'edit', $usersRolesAssignment->user_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Roles Assignment'), ['action' => 'delete', $usersRolesAssignment->user_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersRolesAssignment->user_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Roles Assignments'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Roles Assignment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users Roles'), ['controller' => 'UsersRoles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Role'), ['controller' => 'UsersRoles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersRolesAssignments view large-9 medium-8 columns content">
    <h3><?= h($usersRolesAssignment->user_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $usersRolesAssignment->has('user') ? $this->Html->link($usersRolesAssignment->user->id, ['controller' => 'Users', 'action' => 'view', $usersRolesAssignment->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Users Role') ?></th>
            <td><?= $usersRolesAssignment->has('users_role') ? $this->Html->link($usersRolesAssignment->users_role->id, ['controller' => 'UsersRoles', 'action' => 'view', $usersRolesAssignment->users_role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($usersRolesAssignment->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($usersRolesAssignment->modified) ?></td>
        </tr>
    </table>
</div>
