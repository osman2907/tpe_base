<div class="users-groups index large-9 medium-8 columns content">
    <?= $this->Form->create($usersGroups,['id'=>'listado-users-groups']) ?>
        <h3><?= __('Grupos') ?></h3>
        <br>

        <?php if(isset($permisos['app']['usersgroups']['registrar'])) { ?>
            <?= $this->Html->link(__('Nuevo grupo'), ['action' => 'add'],['id'=>'btn-nuevo-grupo', 'class'=>'btn btn-success']) ?>
        <?php } ?>

        <div id="container-filtros">
            <div class="row filtros-encabezado">
                <!-- Mostrar en resolución de escritorio-->
                <div class="col-sm-12 d-none d-sm-none d-md-block">
                    <?= __('Filtros listado de grupos') ?>
                    <div class="float-right">
                        <button type="button" class="btn btn-danger btn-sm btn-ocultar-filtros hide" data-toggle="tooltip" data-placement="bottom" title="<?= __('Ocultar filtros') ?>" data-original-title="<?= __('Ocultar filtros') ?>"><i class="fa fa-filter"></i> <?= __('Ocultar filtros') ?></button>

                        <button type="button" class="btn btn-info btn-sm btn-mostrar-filtros" data-toggle="tooltip" data-placement="bottom" title="<?= __('Mostrar filtros') ?>" data-original-title="<?= __('Mostrar filtros') ?>"><i class="fa fa-filter"></i> <?= __('Mostrar filtros') ?></button>
                    </div>
                </div>

                <!-- Mostrar en resolución móvil-->
                <div class="col-sm-12 d-block d-sm-block d-md-none filtros-encabezado">
                    <?= __('Filtros') ?>
                    <div class="float-right">
                        <button type="button" class="btn btn-danger btn-sm btn-ocultar-filtros hide" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-filter"></i> <?= __('Ocultar') ?></button>

                        <button type="button" class="btn btn-info btn-sm btn-mostrar-filtros" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-filter"></i> <?= __('Mostrar') ?></button>
                    </div>
                </div>
            </div>

            <div class="row filtros-cuerpo hide">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    echo $this->Form->label("nombre","Nombre",['style'=>'display: block']); 
                    echo $this->Form->text('filtros[nombre]',['class'=>'form-control-sm','style'=>'width:100%']); 
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    $mostrarList=[101=>'Registros activos',102=>'Registros eliminados'];
                    echo $this->Form->label("mostrar","Mostrar",['style'=>'display: block']); 
                    echo $this->Form->select('filtros[mostrar]',$mostrarList,['class'=>'form-control-sm','style'=>'width:100%','empty'=>'Todos','value'=>101]); 
                    ?>
                </div>
            </div>

            <div class="row filtros-pie hide">
                <div class="col-6 col-md-4 col-lg-2">
                    <button id="buscar" type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-search"></i>&nbsp; Buscar
                    </button>
                </div>

                <div class="col-6 col-md-4 col-lg-2">
                    <button id="limpiar" type="reset" class="btn btn-warning btn-sm">
                        <i class="fa fa-eraser"></i>&nbsp; Limpiar
                    </button>
                </div>

                <div class="col-12 d-block d-sm-block d-md-none">
                    <button id="ocultar" type="button" class="btn btn-danger btn-sm btn-ocultar-filtros">
                        <i class="fa fa-filter"></i>&nbsp; Ocultar filtros
                    </button>
                </div>
            </div>
        </div>

        <br>

        <div id="container-listado"></div>
        
    <?= $this->Form->end() ?>

    <?php
    //debug($files);
    ?>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        function cargar_tabla(){
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "usersGroups", "action" => "listar"]); ?>",
                dataType: "html",
                data:$("#listado-users-groups").serialize(),
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#container-listado").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        }
        cargar_tabla();


        $("#btn-nuevo-grupo").click(function(){
            data=$("#listado-users-groups").serialize();
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "usersGroups", "action" => "registrar"]); ?>",
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        });


        $("#buscar").click(function(){
            cargar_tabla();
            return false;
        });


    });
</script>
