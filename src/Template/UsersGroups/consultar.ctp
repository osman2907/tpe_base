<div class="modal fade" id="modal-consultar-group" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Consultar grupo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-consulta1">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="titulo">Grupo</div>
                            <div class="contenido"><?= $userGroup->nombre; ?></div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="titulo">Eliminado</div>
                            <div class="contenido">
                                <?php
                                $clase=$userGroup->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                                ?>
                                <h4><span class="<?= $clase ?>"><?= $userGroup->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                            </div>
                        </div>
                    </div>

                    <div class="row separacion">
                        <div class="col-12">
                            <div class="titulo">Roles asociados a este grupo</div>
                            <div class="contenido">
                                <div class="row">
                                    <?php 
                                    foreach($roles as $rol):
                                        $nombre=$rol->nombre;
                                        ?>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                            <i class="fa fa-check"></i>
                                            <?= $nombre ?>
                                        </div>
                                        <?php
                                    endforeach;

                                    if(count($roles) == 0){
                                        ?>
                                        <div class="alert alert-warning ancho-completo">
                                            Este grupo no ha sido asociado a ningún rol
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-consultar-group").modal('show');
    });
</script>
