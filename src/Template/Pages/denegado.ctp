<div class="text-center">
	<h2>SISTEMA BASE @tupaginaexpress</h2>
	<br>
	<div class="row">
		<div class="col-8 offset-2 col-sm-4 offset-sm-1 col-md-4 offset-md-1 col-lg-6 offset-lg-0 col-xl-4 offset-xl-2">
			<?= $this->Html->image('denegado.png') ?>
		</div>

		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 denegado-contenido">
			<h3>Acceso denegado</h3>
			<p>Usted no tiene la permisología asignada para ejecutar esta acción.</p>
		</div>
	</div>
</div>