<h3><?= __('Crear usuario') ?></h3>

<div class="register-user-wrap">
    <div class="login-content">
        <div class="login-logo">
            <a href="#">
                <?= $this->Html->image('logo512.png',['class'=>'logo-login','style'=>'width:150px;']) ?>
            </a>
        </div>
        <div class="login-form">
            <?= $this->Form->create(); ?>
                <div id="container-register"></div>
            <?= $this->Form->end(); ?>
            <div class="register-link">
                <p>
                    Desarrollado por <b>@tupaginaexpress</b>
                    <a href="https://www.tupaginaexpress.com.ve">Visita nuestro sitio web</a>
                </p>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        cargarIdentificacion=function(){
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "pages", "action" => "register"]); ?>",
                dataType: "html",
                data:{
                    accion:'inicio'
                },
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#container-register").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
        }

        cargarIdentificacion();
    });
</script>