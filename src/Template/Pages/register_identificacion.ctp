<div class="row">
    <div class="col-12">
        <div class="alert alert-info">Por favor ingrese su número de cédula para validar si existe un registro previo en la base de datos.</div>
    </div>

    <div class="col-2">
        <label>&nbsp;</label>
        <select class="form-control" id="prefijo">
            <?php
            foreach ($prefijosList as $clave => $valor){
                ?>
                <option value="<?= $clave ?>"><?= $valor ?></option>
                <?php
            }
            ?>
        </select>
    </div>

    <div class="col-6">
        <label>Cédula</label>
        <input id="cedula" class="au-input au-input--full" type="number" name="cedula" placeholder="Cédula">
    </div>
    
    <div class="col-2">
        <button id="btn-consultar" class="au-btn au-btn--green btn-consultar-login" type="submit">Consultar</button>
    </div>
</div>

<script>
    $("#btn-consultar").click(function(){
        cedula=$("#cedula").val().trim();
        prefijo=$("#prefijo").val().trim();

        if(cedula == ''){
            Swal.fire({
                icon: 'error',
                title: '',
                text: 'Por favor ingrese el número de cédula',
                footer: '',
            });
            return false;
        }
        
        $.ajax({
            type: "POST",
            url: "<?= $this->Url->build(["controller" => "pages", "action" => "register"]); ?>",
            dataType: "json",
            data:{
                prefijo:prefijo,
                cedula:cedula,
                accion:'consultar_identificacion'
            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                if(data.result){
                    formularioPersonas(data);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: '',
                        text: data.mensaje,
                        footer: ''
                    });
                }
                $("#container-listado").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
        return false;
    });

    function formularioPersonas(data) {
		envio={};
		envio.accion='formulario_personas';
		if(data.datos){
			envio.persona_id=data.datos.persona_id;
		}else{
			envio.prefijo=$("#prefijo").val();
			envio.cedula=$("#cedula").val();
		}

		$.ajax({
            type: "POST",
            url: "<?= $this->Url->build(["action" => "register"]); ?>",
            data:envio,
            dataType: "html",
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
            	$(".page-loader").addClass('hide');
            	$("#container-register").html(data);
            },
            error: function (status, error, datos){
            	$(".page-loader").addClass('hide');
            	Swal.fire({
		            icon: 'error',
		            title: '',
		            text: 'Error al consultar, por favor intente nuevamente',
		            footer: ''
		        });
            }
        });
	}
</script>