<div class="login-wrap">
    <div class="login-content">
        <div class="login-logo">
            <a href="#">
                <?= $this->Html->image('logo512.png',['class'=>'logo-login','style'=>'width:150px;']) ?>
            </a>
        </div>
        <div class="login-form">
            <?= $this->Form->create(); ?>
                <div class="form-group">
                    <label>Usuario</label>
                    <input class="au-input au-input--full" type="text" name="usuario" placeholder="Usuario">
                </div>
                <div class="form-group">
                    <label>Contraseña</label>
                    <input class="au-input au-input--full" type="password" name="contrasena" placeholder="Contraseña">
                </div>
                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Ingresar</button>
                <!--<div class="social-login-content">
                    <div class="social-button">
                        <button class="au-btn au-btn--block au-btn--blue m-b-20">sign in with facebook</button>
                        <button class="au-btn au-btn--block au-btn--blue2">sign in with twitter</button>
                    </div>
                </div>-->
            <?= $this->Form->end(); ?>
            <div class="register-link">
                <p>
                    Desarrollado por <b>@tupaginaexpress</b>
                    <a href="https://www.tupaginaexpress.com.ve">Visita nuestro sitio web</a>
                </p>
            </div>
        </div>
    </div>
</div>

<?php
//$this->response->statusCode(201);
?>