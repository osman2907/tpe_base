<?= $this->Form->create($persona,['id'=>'form-editar-persona']) ?>
	<?= $this->Form->control('id'); ?>
	<?= $this->Form->control('accion',['type'=>'hidden','value'=>'editar_persona']); ?>

	<?php
	if(!empty($persona->id)){
		?>
		<div class="alert alert-info">
			Ya existe una persona registrada con esta cédula en el sistema, si los datos son correctos presione el botón <b>Siguiente</b> para continuar con el registro, en caso contrario debe comunicarse con el administrador del sistema para corregirlos.
		</div>
		<?php
	}
	?>
	
	<div class="row">
        <div class="col-12 col-sm-3 col-md-2">
            <?= $this->Form->control('prefijo',['options'=>$prefijosList,'class'=>'form-control']); ?>
		</div>

		<div class="col-12 col-sm-3 col-md-2">
			<?= $this->Form->control('cedula',['class'=>'form-control solo-numero','id'=>'txt-cedula-persona','label'=>'Cédula']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('primer_nombre',['class'=>'form-control']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('segundo_nombre',['class'=>'form-control']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('primer_apellido',['class'=>'form-control']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('segundo_apellido',['class'=>'form-control']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('genero_id', ['options' => $generosList, 'class'=>'form-control','label'=>'Género']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('telefono1',['class'=>'form-control','label'=>'Teléfono 1']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('telefono2',['class'=>'form-control','label'=>'Teléfono 2']); ?>
		</div>

		<div class="col-12 col-sm-6 col-md-4">
			<?= $this->Form->control('correo_electronico',['class'=>'form-control','label'=>'Correo Electrónico']); ?>
		</div>

    </div>
    
    <div class="row separacion">
        <div class="col-12">
            <?php 
            echo $this->Form->button("Anterior",['id'=>'btn-anterior','class'=>'btn btn-warning']);
            echo "&nbsp;&nbsp;&nbsp;";
            if(is_null($persona->id)){
                echo $this->Form->button("Registrar",['id'=>'btn-siguiente','class'=>'btn btn-success']);
            }else{
                echo $this->Form->button("Siguiente",['id'=>'btn-siguiente','class'=>'btn btn-success']);
            }
            ?>
        </div>
    </div>

<?= $this->Form->end() ?>


<script type="text/javascript">
	$("document").ready(function(){

		$("#txt-cedula-persona").focus();

		$("#btn-siguiente").click(function(event){
			editarPersona();
			return false;
		});

		$("#form-editar-persona").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				editarPersona();
			}
		});

		$("#btn-anterior").click(function(){
			$.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["action" => "register"]); ?>",
                data:{
                	accion:'inicio'
                },
                dataType: "html",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                	$(".page-loader").addClass('hide');
	            	$("#container-register").html(data);
                },
                error: function (status, error, datos){
                	$(".page-loader").addClass('hide');
                	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al registrar, por favor intente nuevamente',
			            footer: ''
			        });
                }
            });
			return false;
		});
	});


	function formularioRegistrarUsuario(data){
		envio={};
		envio.accion='formulario_usuarios';
		envio.persona_id=data.datos.persona_id;

		$.ajax({
            type: "POST",
            url: "<?= $this->Url->build(["action" => "register"]); ?>",
            data:envio,
            dataType: "html",
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
            	$(".page-loader").addClass('hide');
            	$("#container-register").html(data);
            },
            error: function (status, error, datos){
            	$(".page-loader").addClass('hide');
            	Swal.fire({
		            icon: 'error',
		            title: '',
		            text: 'Error al consultar, por favor intente nuevamente',
		            footer: ''
		        });
            }
        });
	}


	function editarPersona(){
		data=$("#form-editar-persona").serialize();
		$.ajax({
            type: "POST",
            url: "<?= $this->Url->build(["action" => "register"]); ?>",
            data:data,
            dataType: "json",
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
            	$(".page-loader").addClass('hide');
            	$("label[class='error']").remove();
            	if(data.errors){
            		errores=data.errors;
            		$.each(errores, function(i,mensaje){
            			$("[name='"+i+"']").parent().append('<label class="error">'+mensaje+'</label>');
            		});
            		return false;
            	}

            	if(data.result){
            		formularioRegistrarUsuario(data);
            	}else{
            		Swal.fire({
			            icon: 'error',
			            title: '',
			            text: data.mensaje,
			            footer: ''
			        });
            	}
            },
            error: function (status, error, datos){
            	$(".page-loader").addClass('hide');
            	Swal.fire({
		            icon: 'error',
		            title: '',
		            text: 'Error al actualizar, por favor intente nuevamente',
		            footer: ''
		        });
            }
        });
	}
			

</script>