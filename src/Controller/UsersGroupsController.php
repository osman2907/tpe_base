<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\I18n\I18n;


class UsersGroupsController extends AppController{

    public function listar(){

        // Prior to 3.5 use I18n::locale()
        I18n::locale('es_VE');

        if(isset($this->request['data']['filtros']) || isset($this->request->query['sort'])){
            $this->paginate = [
                'contain' => ['Status'],
                'sortWhitelist' => ['id','nombre'],
                'limit' => 10
            ];

            if(!empty($this->request->data['filtros']['nombre'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(UsersGroups.nombre) LIKE' =>  '%'.strtolower($this->request->data['filtros']['nombre']).'%']];
            }

            if(!empty($this->request->data['filtros']['mostrar'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['UsersGroups.status_id' => $this->request->data['filtros']['mostrar']]];
            }

            $usersGroups = $this->paginate($this->UsersGroups);

            $this->set(compact('usersGroups'));
            $this->render("listar_grupos");
        }

        if(!isset($this->request['data']['filtros'])){
            $usersGroups = $this->UsersGroups;
            $this->set(compact('usersGroups'));
        }
        
    }

    
    public function consultar($id = null){
        $userGroup = $this->UsersGroups->get($id, [
            'contain' => ['Status', 'UsersRoles']
        ]);

        $this->loadModel('UsersRoles');
        $roles = $this->UsersRoles->find()->where(['users_group_id' => $id, 'status_id'=>101])->toArray();

        $this->set(compact('userGroup','roles'));
    }


    public function registrar(){
        if (isset($this->request['data']['filtros'])){
            $userGroup = $this->UsersGroups->newEntity();
            $userGroup->status_id=101; //Activo por defecto
            $this->set(compact('userGroup'));
        }

        if (!isset($this->request['data']['filtros'])){
            if ($this->request->is('post')) {
                $userGroup=$this->UsersGroups->newEntity();
                $userGroup = $this->UsersGroups->patchEntity($userGroup, $this->request->getData());
                if(count($userGroup->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($userGroup->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->UsersGroups->save($userGroup)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Grupo registrado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error registrando grupo']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando grupo']);
            }
            exit();
        }
    }


    public function editar($id = null){
        $userGroup = $this->UsersGroups->get($id, [
            'contain' => ['Status']
        ]);

        if (isset($this->request['data']['filtros'])){
            $this->set(compact('userGroup'));
        }

        if (!isset($this->request['data']['filtros'])){
            if ($this->request->is('put')){
                $userGroup = $this->UsersGroups->patchEntity($userGroup, $this->request->getData());
                if(count($userGroup->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($userGroup->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->UsersGroups->save($userGroup)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Grupo editado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error editando grupo']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando grupo']);
            }
            exit();
        }
    }

    
    public function eliminar($id){
        $eliminar=$this->UsersGroups->eliminarGrupo($id);
        if($eliminar){
            echo json_encode(['result'=>true, 'mensaje'=>'Grupo eliminado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function reciclar($id){
        $reciclar=$this->UsersGroups->reciclarGrupo($id);
        if($reciclar){
            echo json_encode(['result'=>true, 'mensaje'=>'Grupo reciclado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }
    
}
