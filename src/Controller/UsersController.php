<?php
namespace App\Controller;

use App\Controller\AppController;

class UsersController extends AppController{

    
    public function listar(){
        
        if(isset($this->request['data']['filtros']) || isset($this->request->query['sort'])){
            $this->paginate = [
                'contain' => ['Status','Personas'],
                'sortWhitelist' => ['id','usuario','role','Personas.cedula','Personas.primer_nombre'],
                'limit' => 10
            ];

            if(!empty($this->request->data['filtros']['prefijo'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Personas.prefijo' => $this->request->data['filtros']['prefijo']]];
            }

            if(!empty($this->request->data['filtros']['cedula'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Personas.cedula' =>  $this->request->data['filtros']['cedula']]];
            }

            if(!empty($this->request->data['filtros']['primer_nombre'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(Personas.primer_nombre) LIKE' => '%'.strtolower($this->request->data['filtros']['primer_nombre']).'%' ]];
            }

            if(!empty($this->request->data['filtros']['primer_apellido'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(Personas.primer_apellido) LIKE' => '%'.strtolower($this->request->data['filtros']['primer_apellido']).'%' ]];
            }

            if(!empty($this->request->data['filtros']['usuario'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(Users.usuario) LIKE' =>  '%'.strtolower($this->request->data['filtros']['usuario']).'%']];
            }

            if(!empty($this->request->data['filtros']['role'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Users.role' => $this->request->data['filtros']['role']]];
            }

            if(!empty($this->request->data['filtros']['mostrar'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Users.status_id' => $this->request->data['filtros']['mostrar']]];
            }
            $users = $this->paginate($this->Users);

            $this->set(compact('users'));
            $this->render("listar_usuarios");
        }

        if(!isset($this->request['data']['filtros'])){
            $users = $this->Users;
            $roleList = $this->listarRoles();
            $prefijosList = $this->listarPrefijosPersonas();
            $this->set(compact('users','roleList','prefijosList'));
        }
    }

    
    public function consultar($id = null){
        $user = $this->Users->get($id, ['contain' => ['Status','Personas']]);

        $this->loadModel('UsersRolesAssignments');

        $roles=$this->UsersRolesAssignments->consultarRolesAsignados($id);

        $this->set(compact('user','roles'));
    }

    
    public function registrar(){
        $user = $this->Users->newEntity();
        $user->status_id=101; //Activo por defecto
        $this->loadModel('Personas.Personas');
        $this->loadModel('Descripcions');

        if ($this->request['data']['accion'] == 'administrar'){
            $this->set(compact('user'));
            $this->render('registrar');
        }

        if ($this->request['data']['accion'] == 'registrar'){
            $prefijosList = $this->listarPrefijosPersonas();
            $this->viewBuilder()->setLayout('ajax');
            $this->set(compact('user','prefijosList'));
            $this->render('registrar_identificacion');
        }

        //Buscar si el usuario y la persona existen en la base de datos...
        if ($this->request['data']['accion'] == 'registrar_identificacion'){
            $prefijo=$_POST['prefijo'];
            $cedula=$_POST['cedula'];
            
            //Primero validamos si existe otro usuario registrado con la cédula ingresada...
            $validarUser=$this->Users->validarUsuarioIdentificacion($prefijo,$cedula);
            if(count($validarUser) > 0){
                echo json_encode(['result'=>false, 'mensaje'=>'Ya existe un usuario registrado con esta cédula']);
                exit();
            }

            //Ahora validamos si la persona existe pero no está asociada a ningún usuario...
            $validarPersona=$this->Personas->validarPersonaIdentificacion($prefijo,$cedula);
            if(count($validarPersona) > 0){
                echo json_encode(['result'=>true, 'datos'=>['persona_id'=>$validarPersona[0]->id]]);
                exit();
            }else{
                echo json_encode(['result'=>true]);
                exit();
            }
            exit();
        }

        if ($this->request['data']['accion'] == 'formulario_personas'){
            if(isset($_POST['persona_id'])){
                $idPersona=$_POST['persona_id'];
                $persona=$this->Personas->get($idPersona);
            }else{
                $persona = $this->Personas->newEntity();
                $persona->prefijo=$_POST['prefijo'];
                $persona->cedula=$_POST['cedula'];
                $persona->accion="editar_persona";
            }
            $prefijosList = $this->listarPrefijosPersonas();
            $generosList = $this->Descripcions->find('list', ['limit' => 200])->where(['padre_id'=>'10']);
            $this->set(compact('persona','prefijosList','generosList'));
            $this->render('editar_persona');
        }

        if ($this->request['data']['accion'] == 'editar_persona'){

            if ($this->request->is('post')) {
                $persona = $this->Personas->newEntity();
            }else{
                $id=$this->request->getData('id');
                $persona = $this->Personas->get($id);
            }

            $persona = $this->Personas->patchEntity($persona, $this->request->getData());
            if(count($persona->errors()) > 0){ //Validación a través del modelo...
                $errores=$this->formatearErrores($persona->errors());
                echo json_encode(['result'=>false, 'errors' => $errores]);
                exit();
            }
            if ($this->Personas->save($persona)){
                echo json_encode(['result'=>true, 'datos'=>['persona_id'=>$persona->id]]);
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error gestionando persona']);
            }
            exit();
        }

        if ($this->request['data']['accion'] == 'formulario_usuarios'){
            $idPersona=$_POST['persona_id'];
            
            $persona=$this->Personas->get($idPersona);
            $user->persona_id=$_POST['persona_id'];
            $user->cedula=$persona->cedula;
            $user->primer_nombre=$persona->primer_nombre;
            $user->primer_apellido=$persona->primer_apellido;

            $roleList = $this->listarRoles();
            $this->set(compact('user', 'roleList'));
            $this->render('registrar_usuario');
        }

        if ($this->request['data']['accion'] == 'registrar_usuario'){
            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                if(count($user->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($user->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Users->save($user)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Usuario registrado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error registrando usuario']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando usuario']);
            }
            exit();
        }
    }

    
    public function editar($id = null){
        $user = $this->Users->get($id, [
            'contain' => ['Status','Personas']
        ]);

        if (!isset($this->request['data']['id'])) { //Carga del formulario...
            $user->cedula=strtoupper($user->persona->prefijo)."-".$user->persona->cedula;
            $user->primer_nombre=$user->persona->primer_nombre;
            $user->primer_apellido=$user->persona->primer_apellido;
            $roleList = $this->listarRoles();
            $this->set(compact('user','roleList'));
        }else{ //Datos que envía el formulario para editar....
            $id=$this->request->getData('id');
            $user = $this->Users->get($id);
            if ($this->request->is('put')){
                $user = $this->Users->patchEntity($user, $this->request->getData());
                if(count($user->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($user->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Users->save($user)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Usuario editado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error editando usuario']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando usuario']);
            }
            exit();
        }
    }


    public function eliminar($id){
        $eliminar=$this->Users->eliminarUsuario($id);
        if($eliminar){
            echo json_encode(['result'=>true, 'mensaje'=>'Usuario eliminado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function reciclar($id){
        $reciclar=$this->Users->reciclarUsuario($id);
        if($reciclar){
            echo json_encode(['result'=>true, 'mensaje'=>'Usuario reciclado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function editarContrasena($id = null){
        $user = $this->Users->get($id, ['contain' => ['Status','Personas']]);
        if (!isset($this->request['data']['id'])) { //Carga del formulario...
            $user->cedula=strtoupper($user->persona->prefijo)."-".$user->persona->cedula;
            $user->primer_nombre=$user->persona->primer_nombre;
            $user->primer_apellido=$user->persona->primer_apellido;
            $this->set(compact('user'));
        }else{
            $id=$this->request->getData('id');
            $user = $this->Users->get($id);
            if ($this->request->is('put')){
                $user = $this->Users->patchEntity($user, $this->request->getData());
                if(count($user->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($user->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Users->save($user)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Contraseña editada exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error editando contraseña']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando contraseña']);
            }
            exit();
        }
    }


    public function asignarRoles($id = null){
        $user = $this->Users->get($id, ['contain' => ['Status','Personas']]);
        $this->loadModel('UsersRolesAssignments');

        if (!isset($this->request['data']['id'])) { //Carga del formulario...
            $user->cedula=strtoupper($user->persona->prefijo)."-".$user->persona->cedula;
            $user->primer_nombre=$user->persona->primer_nombre;
            $user->primer_apellido=$user->persona->primer_apellido;

            $asignadas=$this->UsersRolesAssignments->consultarAsignadas($id);
            $this->loadModel('UsersRoles');
            $rolesList=$this->UsersRoles->listarRolesGrupos();
            $this->set(compact('user','rolesList','asignadas'));
        }else{
            $idUser=$this->request->getData('id');
            $user = $this->Users->get($idUser);
            $roles=isset($_POST['roles']) ? $_POST['roles'] : [];
            
            $actualizar=$this->UsersRolesAssignments->actualizarRoles($idUser,$roles);
            if($actualizar){
                echo json_encode(['result'=>true, 'mensaje'=>'Roles actualizados con éxito']);
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
            }

            exit();
        }
    }

}
