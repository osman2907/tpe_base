<?php
namespace App\Controller;
use App\Controller\AppController;

class PeriodosAcademicosController extends AppController{
    
    public function index(){
        $this->paginate = [
            'contain' => ['Estatus','Status']
            //'limit' => 5
        ];
        $periodosAcademicos = $this->paginate($this->PeriodosAcademicos);

        $this->set(compact('periodosAcademicos'));
    }

    
    public function view($id = null){
        $periodosAcademico = $this->PeriodosAcademicos->get($id, [
            'contain' => ['Estatus','Status']
        ]);

        $this->set('periodosAcademico', $periodosAcademico);
    }


    public function nuevoPeriodoAcademico(){
        $periodosAcademicos = $this->PeriodosAcademicos->newEntity();
        $periodosAcademicos->status_id=101; //Activo por defecto
        $estatusList = $this->PeriodosAcademicos->Estatus->find('list', ['limit' => 200])->where(['padre_id'=>1]);
        $this->set(compact('periodosAcademicos', 'estatusList'));
    }


    public function crearPeriodoAcademico(){
        $periodosAcademico = $this->PeriodosAcademicos->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['fecha_inicio']=!empty($this->request->data['fecha_inicio']) ? $this->convertirFecha($this->request->data['fecha_inicio']) : "";
            $this->request->data['fecha_fin']=!empty($this->request->data['fecha_fin']) ? $this->convertirFecha($this->request->data['fecha_fin']) : "";
            $periodosAcademico = $this->PeriodosAcademicos->patchEntity($periodosAcademico, $this->request->getData());
            if(count($periodosAcademico->errors()) > 0){ //Validación a través del modelo...
                $errores=$this->formatearErrores($periodosAcademico->errors());
                echo json_encode(['result'=>false, 'errors' => $errores]);
                exit();
            }
            if ($this->PeriodosAcademicos->save($periodosAcademico)){
                echo json_encode(['result'=>true, 'mensaje'=>'Período académico registrado exitosamente']);
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando período académico']);
            }
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Error registrando período académico']);
        }
        exit();
    }


    public function editarPeriodoAcademico($id = null){
        $periodosAcademicos = $this->PeriodosAcademicos->get($id, [
            'contain' => ['Estatus','Status']
        ]);
        $this->set(compact('periodosAcademicos'));
    }


    public function actualizarPeriodoAcademico(){
        $id=$this->request->getData('id');
        $periodosAcademico = $this->PeriodosAcademicos->get($id);
        if ($this->request->is('put')) {
            $this->request->data['fecha_inicio']=!empty($this->request->data['fecha_inicio']) ? $this->convertirFecha($this->request->data['fecha_inicio']) : "";
            $this->request->data['fecha_fin']=!empty($this->request->data['fecha_fin']) ? $this->convertirFecha($this->request->data['fecha_fin']) : "";
            $periodosAcademico = $this->PeriodosAcademicos->patchEntity($periodosAcademico, $this->request->getData());
            if(count($periodosAcademico->errors()) > 0){ //Validación a través del modelo...
                $errores=$this->formatearErrores($periodosAcademico->errors());
                echo json_encode(['result'=>false, 'errors' => $errores]);
                exit();
            }
            if ($this->PeriodosAcademicos->save($periodosAcademico)){
                echo json_encode(['result'=>true, 'mensaje'=>'Período académico editado exitosamente']);
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando período académico']);
            }
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Error editando período académico']);
        }
        exit();
    }


    public function desactivarPeriodoAcademico($id){
        $validacion=$this->PeriodosAcademicos->validarPeriodoAcademicoActivo($id);

        if(!$validacion){
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, período académico desactivado previamente']);
            exit();
        }

        $desactivar=$this->PeriodosAcademicos->desactivarPeriodoAcademico($id);
        if($desactivar){
            echo json_encode(['result'=>true, 'mensaje'=>'Período académico desactivado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function activarPeriodoAcademico($id){
        $validacion=$this->PeriodosAcademicos->validarPeriodoAcademicoInactivo($id);

        if(!$validacion){
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, período académico activado previamente']);
            exit();
        }

        $activar=$this->PeriodosAcademicos->activarPeriodoAcademico($id);
        if($activar){
            echo json_encode(['result'=>true, 'mensaje'=>'Período académico activado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function eliminarPeriodoAcademico($id){
        $eliminar=$this->PeriodosAcademicos->eliminarPeriodoAcademico($id);
        if($eliminar){
            echo json_encode(['result'=>true, 'mensaje'=>'Período académico eliminado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function reciclarPeriodoAcademico($id){
        $reciclar=$this->PeriodosAcademicos->reciclarPeriodoAcademico($id);
        if($reciclar){
            echo json_encode(['result'=>true, 'mensaje'=>'Período académico reciclado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    /*public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $periodosAcademico = $this->PeriodosAcademicos->get($id);
        if ($this->PeriodosAcademicos->delete($periodosAcademico)){
            $this->Flash->success(__('The periodos academico has been deleted.'));
        }else{
            $this->Flash->error(__('The periodos academico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }*/

}
