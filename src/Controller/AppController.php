<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use ReflectionClass;
use ReflectionMethod;



class AppController extends Controller{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');


        $this->loadComponent('Auth',[
            'authorize'=>['Controller'],
            'authenticate'=>[
                'Form'=>[
                    'fields'=>[
                        'username'=>'usuario',
                        'password'=>'contrasena'
                    ]
                ]
            ],
            'loginAction'=>[
                'controller'=>'Pages',
                'action'=>'login'
            ],
            'authError'=>'Datos incorrectos',
            'loginRedirect'=>[
                'controller'=> 'Pages',
                'action'=>'inicio'
            ],
            /*'logoutRedirect'=>[
                'controller'=> 'Users',
                'action'=>'login'
            ],*/
            //'unauthorized'=> $this->referer()
            'unauthorized'=> [
                'controller'=>'prueba',
                'action'=>'accion'
            ]
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }


    public function formatearErrores($errores){
        $listado=[];
        foreach ($errores as $campo => $error){
            $listado[$campo]=current($error);
        }
        return $listado;
    }


    public function convertirFecha($fecha){
        $elementos=explode("/", $fecha);
        $retorno=$elementos[2]."-".$elementos[1]."-".$elementos[0];
        return $retorno;
    }


    public function listarRoles(){
        return ['usuario'=>'Usuario','administrador'=>'Administrador'];
    }


    public function listarPrefijosPersonas(){
        return ['v'=>'V','e'=>'E'];
    }


    public function listarPrefijosEmpresas(){
        return ['j'=>'J','g'=>'G'];
    }


    public function beforeFilter(Event $event){
        $this->loadModel('Personas.Personas');

        $user=$this->Auth->user();

        $this->set("current_user",$user);

        if(!$this->Auth->user()){
            if($this->request->action != 'login'){
                $this->redirect(['plugin'=>false, 'controller'=>'pages','action' => 'login']);
            }
        }else{
            unset($_SESSION['Flash']);
        }
    }

    public function isAuthorized($user){
        $this->loadModel('Users');

        $user=$this->Auth->user();
        $idUser=$user['id'];

        //debug("eliminando"); die;


        if(isset($user['role']) && ($user['role'] === 'administrador' || $user['role'] === 'usuario')){
            if($user['role'] === 'usuario'){
                $permisos=$this->Users->obtenerRutasPermitidas($idUser);
                $this->set("permisos",$permisos);
                $plugin=is_null($this->request->plugin) ? 'app' :  strtolower($this->request->plugin);
                $controller=  strtolower($this->request->controller);
                $action=  strtolower($this->request->action);
                
                if(isset($permisos[$plugin][$controller][$action])){
                    return true;
                }elseif($this->request->action == 'denegado'){
                    return true;
                }else{
                    return $this->redirect(['plugin'=>null, 'controller'=>'Pages', 'action' => 'denegado']);
                }
            }

            if($user['role'] === 'administrador'){
                $rutas=$this->obtenerRutas();
                $permisos=$this->Users->asignarRutasTodas($rutas);
                $this->set("permisos",$permisos);
                return true;
            }

        }

        if($this->request->action != 'denegado'){
            return $this->redirect(['plugin'=>null, 'controller'=>'Pages', 'action' => 'denegado']);
        }

    }


    public function getControllers(){
        $files = scandir('../src/Controller/');
        $results = [];
        $ignoreList = [
            '.', 
            '..', 
            'Component', 
            'AppController.php',
            'ErrorController.php',
            // 'PagesController.php',
        ];
        foreach($files as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];

                $results[]= [
                    'plugin' => "App",
                    'controller' => str_replace('Controller', '', $controller)
                ];
            }
        }

        $filesPlugins = scandir('../plugins/');
        $ignoreListPlugins = [
            '.', 
            '..'
        ];
        foreach($filesPlugins as $filePlugin){
            if(!in_array($filePlugin, $ignoreListPlugins)) {
                $plugin = explode('.', $filePlugin)[0];

                $files = scandir("../plugins/$plugin/src/Controller");
                $ignoreList = [
                    '.', 
                    '..', 
                    'Component', 
                    'AppController.php',
                    'ErrorController.php',
                ];

                foreach($files as $file){
                    if(!in_array($file, $ignoreList)) {
                        $controller = explode('.', $file)[0];

                        $results[]= [
                            'plugin' => $plugin,
                            'controller' => str_replace('Controller', '', $controller)
                        ];
                    }
                }
            }
        }
        return $results;
    }

    public function getActions($plugin, $controllerName) {
        if($plugin == 'App'){
            $className = 'App\\Controller\\'.$controllerName.'Controller';
        }else{
            $className = $plugin.'\\Controller\\'.$controllerName.'Controller';
        }
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $results = [];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach($actions as $action){
            if($action->class == $className && !in_array($action->name, $ignoreList)){
                $results[]=$action->name;
            }
        }
        return $results;
    }


    public function obtenerRutas(){
        $controllers = $this->getControllers();
        $resources = [];
        foreach($controllers as $key => $fila){
            $actions = $this->getActions($fila['plugin'], $fila['controller']);
            $controllers[$key]['acciones']=$actions;
        }
        return $controllers;
    }
}
