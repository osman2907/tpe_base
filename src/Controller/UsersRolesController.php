<?php
namespace App\Controller;

use App\Controller\AppController;


class UsersRolesController extends AppController{

    public function listar(){
        
        if(isset($this->request['data']['filtros']) || isset($this->request->query['sort'])){
            $this->paginate = [
                'contain' => ['Status', 'UsersGroups'],
                'sortWhitelist' => ['id','nombre','UsersGroups.nombre'],
                'limit' => 10
            ];

            if(!empty($this->request->data['filtros']['nombre'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(UsersRoles.nombre) LIKE' =>  '%'.strtolower($this->request->data['filtros']['nombre']).'%']];
            }

            if(!empty($this->request->data['filtros']['users_group_id'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['UsersRoles.users_group_id' => $this->request->data['filtros']['users_group_id']]];
            }

            if(!empty($this->request->data['filtros']['mostrar'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['UsersRoles.status_id' => $this->request->data['filtros']['mostrar']]];
            }
            $usersRoles = $this->paginate($this->UsersRoles);

            $this->set(compact('usersRoles'));
            $this->render("listar_roles");
        }

        if(!isset($this->request['data']['filtros'])){
            $usersRoles = $this->UsersRoles;
            $gruposList = $this->UsersRoles->UsersGroups->find('list', ['limit' => 200])->where(['status_id'=>101])->order(['UsersGroups.nombre']);
            $this->set(compact('usersRoles','gruposList'));
        }
    }

    
    public function consultar($id = null){
        $userRole = $this->UsersRoles->get($id, [
            'contain' => ['Status', 'UsersGroups', 'UsersRolesAssignments', 'UsersRolesRoutes']
        ]);

        $asignadas=$this->UsersRoles->UsersRolesRoutes->consultarAsignadas($id);
        $asignadas=$this->UsersRoles->agruparAsignadas($asignadas);

        $this->set(compact('userRole','asignadas'));
    }


    public function registrar(){

        $userRole = $this->UsersRoles->newEntity();

        if (isset($this->request['data']['filtros'])){
            $userRole->status_id=101; //Activo por defecto

            $gruposList = $this->UsersRoles->UsersGroups->find('list', ['limit' => 200])->where(['status_id'=>101]);
            $this->set(compact('userRole','gruposList'));
        }

        if (!isset($this->request['data']['filtros'])){
            if ($this->request->is('post')) {
                $userRole = $this->UsersRoles->patchEntity($userRole, $this->request->getData());
                if(count($userRole->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($userRole->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->UsersRoles->save($userRole)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Rol registrado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error registrando rol']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando rol']);
            }
            exit();
        }

    }


    public function editar($id = null){
        $userRole = $this->UsersRoles->get($id, ['contain' => ['Status']]);
        
        if (isset($this->request['data']['filtros'])){
            $gruposList = $this->UsersRoles->UsersGroups->find('list', ['limit' => 200])->where(['status_id'=>101]);
            $this->set(compact('userRole','gruposList'));
        }

        if (!isset($this->request['data']['filtros'])){
            if ($this->request->is('put')){
                $userRole = $this->UsersRoles->patchEntity($userRole, $this->request->getData());
                if(count($userRole->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($userRole->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->UsersRoles->save($userRole)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Rol editado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error editando rol']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando rol']);
            }
            exit();
        }

    }


    public function eliminar($id){
        $eliminar=$this->UsersRoles->eliminarRol($id);
        if($eliminar){
            echo json_encode(['result'=>true, 'mensaje'=>'Rol eliminado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function reciclar($id){
        $reciclar=$this->UsersRoles->reciclarRol($id);
        if($reciclar){
            echo json_encode(['result'=>true, 'mensaje'=>'Rol reciclado con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function editarPermisos($id = null){
        if(!isset($_POST['id'])){
            $userRole = $this->UsersRoles->get($id, [
                'contain' => ['Status','UsersGroups']
            ]);
            
            $asignadas=$this->UsersRoles->UsersRolesRoutes->consultarAsignadas($id);

            $rutas=$this->obtenerRutas();
            $this->set(compact('userRole','rutas','asignadas'));
        }

        if(isset($_POST['id'])){
            $permisos=isset($_POST['rutas']) ? $_POST['rutas'] : [];
            $idRol=$_POST['id'];
            $actualizar=$this->UsersRoles->UsersRolesRoutes->actualizarPermisos($idRol,$permisos);
            if($actualizar){
                echo json_encode(['result'=>true, 'mensaje'=>'Permisos actualizados con éxito']);
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
            }
            exit();
        }
    }

}
