<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersRolesAssignments Controller
 *
 * @property \App\Model\Table\UsersRolesAssignmentsTable $UsersRolesAssignments
 *
 * @method \App\Model\Entity\UsersRolesAssignment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersRolesAssignmentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'UsersRoles']
        ];
        $usersRolesAssignments = $this->paginate($this->UsersRolesAssignments);

        $this->set(compact('usersRolesAssignments'));
    }

    /**
     * View method
     *
     * @param string|null $id Users Roles Assignment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersRolesAssignment = $this->UsersRolesAssignments->get($id, [
            'contain' => ['Users', 'UsersRoles']
        ]);

        $this->set('usersRolesAssignment', $usersRolesAssignment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersRolesAssignment = $this->UsersRolesAssignments->newEntity();
        if ($this->request->is('post')) {
            $usersRolesAssignment = $this->UsersRolesAssignments->patchEntity($usersRolesAssignment, $this->request->getData());
            if ($this->UsersRolesAssignments->save($usersRolesAssignment)) {
                $this->Flash->success(__('The users roles assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users roles assignment could not be saved. Please, try again.'));
        }
        $users = $this->UsersRolesAssignments->Users->find('list', ['limit' => 200]);
        $usersRoles = $this->UsersRolesAssignments->UsersRoles->find('list', ['limit' => 200]);
        $this->set(compact('usersRolesAssignment', 'users', 'usersRoles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Roles Assignment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersRolesAssignment = $this->UsersRolesAssignments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersRolesAssignment = $this->UsersRolesAssignments->patchEntity($usersRolesAssignment, $this->request->getData());
            if ($this->UsersRolesAssignments->save($usersRolesAssignment)) {
                $this->Flash->success(__('The users roles assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users roles assignment could not be saved. Please, try again.'));
        }
        $users = $this->UsersRolesAssignments->Users->find('list', ['limit' => 200]);
        $usersRoles = $this->UsersRolesAssignments->UsersRoles->find('list', ['limit' => 200]);
        $this->set(compact('usersRolesAssignment', 'users', 'usersRoles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Roles Assignment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersRolesAssignment = $this->UsersRolesAssignments->get($id);
        if ($this->UsersRolesAssignments->delete($usersRolesAssignment)) {
            $this->Flash->success(__('The users roles assignment has been deleted.'));
        } else {
            $this->Flash->error(__('The users roles assignment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
