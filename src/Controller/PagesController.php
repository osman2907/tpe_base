<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;


class PagesController extends AppController{

    public function beforeFilter(Event $event){

        if($this->request->action == "register" || $this->request->action == "recovery"){
            $this->Auth->allow(['register','recovery']);
        }else{
            parent::beforeFilter($event);
        }

    }


    public function display(...$path){
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }


    public function login(){
        $this->loadModel('Personas.Personas');
        if($this->request->is('post')){
            unset($_SESSION['Flash']);
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                $persona=$this->Personas->get($user['persona_id']);

                $session = $this->request->session();
                $session->write('Auth.User.nombre', $persona->primer_nombre);
                $session->write('Auth.User.apellido', $persona->primer_apellido);
                $session->write('Auth.User.correo_electronico', $persona->correo_electronico);
                
                return $this->redirect($this->Auth->redirectUrl());
            }else{
                $this->Flash->error("Usuario y/o contraseña incorrectos. Por favor Intente nuevamente",['key'=>'auth']);
            }
        }
    }


    public function logout(){
        return $this->redirect($this->Auth->logout());
    }
    

    public function inicio(){
        
    }


    public function denegado(){
        
    }


    public function register(){
        $this->loadModel('Personas.Personas');
        $this->loadModel('Users');
        $this->loadModel('Descripcions');

        if (isset($this->request['data']['accion']) && $this->request['data']['accion'] == 'inicio'){
            $prefijosList = $this->listarPrefijosPersonas();
            $this->set(compact('prefijosList'));
            $this->render('register_identificacion');
        }

        if (isset($this->request['data']['accion']) && $this->request['data']['accion'] == 'consultar_identificacion'){
            $prefijo=$this->request['data']['prefijo'];
            $cedula=$this->request['data']['cedula'];

            //Primero validamos si existe otro usuario registrado con la cédula ingresada...
            $validarUser=$this->Users->validarUsuarioIdentificacion($prefijo,$cedula);
            if(count($validarUser) > 0){
                echo json_encode(['result'=>false, 'mensaje'=>'Ya existe un usuario registrado con esta cédula']);
                exit();
            }

            //Ahora validamos si la persona existe pero no está asociada a ningún usuario...
            $this->loadModel('Personas.Personas');
            $validarPersona=$this->Personas->validarPersonaIdentificacion($prefijo,$cedula);
            if(count($validarPersona) > 0){
                echo json_encode(['result'=>true, 'datos'=>['persona_id'=>$validarPersona[0]->id]]);
                exit();
            }else{
                echo json_encode(['result'=>true]);
                exit();
            }

            exit();
        }

        if (isset($this->request['data']['accion']) && $this->request['data']['accion'] == 'formulario_personas'){
            if(isset($_POST['persona_id'])){
                $idPersona=$_POST['persona_id'];
                $persona=$this->Personas->get($idPersona);
            }else{
                $persona = $this->Personas->newEntity();
                $persona->prefijo=$_POST['prefijo'];
                $persona->cedula=$_POST['cedula'];
                $persona->accion="editar_persona";
            }
            $prefijosList = $this->listarPrefijosPersonas();
            $generosList = $this->Descripcions->find('list', ['limit' => 200])->where(['padre_id'=>'10']);
            $this->set(compact('persona','prefijosList','generosList'));
            $this->render('editar_persona');
        }


        if (isset($this->request['data']['accion']) && $this->request['data']['accion'] == 'editar_persona'){
            if ($this->request->is('post')) {
                $persona = $this->Personas->newEntity();
            }else{
                $id=$this->request->getData('id');
                $persona = $this->Personas->get($id);
            }

            $persona = $this->Personas->patchEntity($persona, $this->request->getData());
            if(count($persona->errors()) > 0){ //Validación a través del modelo...
                $errores=$this->formatearErrores($persona->errors());
                echo json_encode(['result'=>false, 'errors' => $errores]);
                exit();
            }
            if ($this->Personas->save($persona)){
                echo json_encode(['result'=>true, 'datos'=>['persona_id'=>$persona->id]]);
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error gestionando persona']);
            }
            exit();
        }


        if (isset($this->request['data']['accion']) && $this->request['data']['accion'] == 'formulario_usuarios'){
            $user = $this->Users->newEntity();
            $user->status_id=101; //Activo por defecto

            $idPersona=$_POST['persona_id'];

            $persona=$this->Personas->get($idPersona);
            $user->persona_id=$_POST['persona_id'];
            $user->cedula=$persona->cedula;
            $user->primer_nombre=$persona->primer_nombre;
            $user->primer_apellido=$persona->primer_apellido;

            $roleList = $this->listarRoles();
            $this->set(compact('user', 'roleList'));
            $this->render('register_usuario');
        }


        if (isset($this->request['data']['accion']) && $this->request['data']['accion'] == 'registrar_usuario'){
            $user = $this->Users->newEntity();
            if ($this->request->is('post')) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                if(count($user->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($user->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Users->save($user)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Usuario registrado exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error registrando usuario']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando usuario']);
            }
            exit();
        }


    }


    public function recovery(){
        
    }


}
