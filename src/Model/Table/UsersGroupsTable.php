<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersGroupsTable extends Table{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users_groups');
        $this->setDisplayField('nombre');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Status', [
            'className' => 'Descripcions',
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UsersRoles', [
            'foreignKey' => 'users_group_id'
        ]);
    }


    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 100)
            ->requirePresence('nombre', 'create')
            ->notEmptyString('nombre','Ingrese nombre.');

        return $validator;
    }


    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }


    public function eliminarGrupo($id){
        $user=$this->get($id);
        $user->status_id=102;
        return $this->save($user) ? true : false;
    }


    public function reciclarGrupo($id){
        $user=$this->get($id);
        $user->status_id=101;
        return $this->save($user) ? true : false;
    }

}
