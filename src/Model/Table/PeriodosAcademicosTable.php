<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;


/**
 * PeriodosAcademicos Model
 *
 * @property \App\Model\Table\DescripcionsTable&\Cake\ORM\Association\BelongsTo $Descripcions
 * @property \App\Model\Table\DescripcionsTable&\Cake\ORM\Association\BelongsTo $Descripcions
 *
 * @method \App\Model\Entity\PeriodosAcademico get($primaryKey, $options = [])
 * @method \App\Model\Entity\PeriodosAcademico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PeriodosAcademico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PeriodosAcademico|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PeriodosAcademico saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PeriodosAcademico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PeriodosAcademico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PeriodosAcademico findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PeriodosAcademicosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('periodos_academicos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Estatus', [
            'className' => 'Descripcions',
            'foreignKey' => 'estatus_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Status', [
            'className' => 'Descripcions',
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 100)
            ->requirePresence('nombre', 'create')
            ->notEmptyString('nombre');

        $validator
            ->date('fecha_inicio')
            ->requirePresence('fecha_inicio', 'create')
            ->notEmptyDate('fecha_inicio');

        $validator
            ->date('fecha_fin')
            ->requirePresence('fecha_fin', 'create')
            ->notEmptyDate('fecha_fin');

        return $validator;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['estatus_id'], 'Estatus'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }


    public function validarPeriodoAcademicoActivo($id){
        $periodo=$this->get($id);
        return $periodo->estatus_id==2 ? true : false;
    }


    public function validarPeriodoAcademicoInactivo($id){
        $periodo=$this->get($id);
        return $periodo->estatus_id==3 ? true : false;
    }


    public function desactivarPeriodoAcademico($id){
        $periodo=$this->get($id);
        $periodo->estatus_id=3;
        return $this->save($periodo) ? true : false;
    }


    public function activarPeriodoAcademico($id){
        $tablename = TableRegistry::get("periodos_academicos");
        $conditions = array('id NOT IN'=>$id);
        $fields = array('estatus_id'=>3);
        $tablename->updateAll($fields, $conditions);

        $periodo=$this->get($id);
        $periodo->estatus_id=2;
        return $this->save($periodo) ? true : false;
    }


    public function eliminarPeriodoAcademico($id){
        $periodo=$this->get($id);
        $periodo->estatus_id=3;
        $periodo->status_id=102;
        return $this->save($periodo) ? true : false;
    }


    public function reciclarPeriodoAcademico($id){
        $periodo=$this->get($id);
        $periodo->status_id=101;
        return $this->save($periodo) ? true : false;
    }

}
