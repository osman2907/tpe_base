<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersRolesAssignmentsTable extends Table{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users_roles_assignments');
        $this->setDisplayField('user_id');
        $this->setPrimaryKey(['user_id', 'users_role_id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UsersRoles', [
            'foreignKey' => 'users_role_id',
            'joinType' => 'INNER'
        ]);
    }


    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['users_role_id'], 'UsersRoles'));

        return $rules;
    }


    public function consultarAsignadas($idUser){
        $asignadas = $this->find()->where(['user_id' => $idUser])->all();
        $retorno=[];

        foreach ($asignadas as $fila){
            $idRol=$fila['users_role_id'];
            $retorno[$idRol]=$idRol;
        }

        return $retorno;
    }


    public function consultarRolesAsignados($idUser){
        $campos=[
            'UsersRoles.id',
            'UsersRoles.nombre'
        ];
        $roles = $this->find()->select($campos)->where(['user_id' => $idUser, 'status_id'=>101])->contain(['UsersRoles'])->toArray();
        return $roles;
    }


    public function actualizarRoles($idUser,$roles){
        $actuales=$this->find('list',['keyField' => 'users_role_id','valueField' => 'users_role_id'])
            ->where(['user_id'=>$idUser])->toArray();
        

        foreach ($actuales as $idRol){
            if(!in_array($idRol, $roles)){
                $this->deleteAll(['user_id'=>$idUser,'users_role_id'=>$idRol]);
            }
        }

        foreach ($roles as $idRol => $valor){
            $datos[]=[
                'user_id'=>$idUser,
                'users_role_id'=>$idRol
            ];  
        }

        $entities = $this->newEntities($datos);
        return $this->saveMany($entities) ? true : false;
    }

}
