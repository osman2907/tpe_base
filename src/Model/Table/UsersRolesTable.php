<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class UsersRolesTable extends Table{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users_roles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Status', [
            'className' => 'Descripcions',
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UsersGroups', [
            'foreignKey' => 'users_group_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UsersRolesAssignments', [
            'foreignKey' => 'users_role_id'
        ]);
        $this->hasMany('UsersRolesRoutes', [
            'foreignKey' => 'users_role_id'
        ]);
    }

    
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 100)
            ->requirePresence('nombre', 'create')
            ->notEmptyString('nombre','Ingrese nombre');

        $validator
            ->scalar('users_group_id')
            ->requirePresence('users_group_id', 'create')
            ->notEmpty('users_group_id','Seleccione grupo.');

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        $rules->add($rules->existsIn(['users_group_id'], 'UsersGroups'));

        return $rules;
    }


    public function eliminarRol($id){
        $user=$this->get($id);
        $user->status_id=102;
        return $this->save($user) ? true : false;
    }


    public function reciclarRol($id){
        $user=$this->get($id);
        $user->status_id=101;
        return $this->save($user) ? true : false;
    }


    public function listarRolesGrupos(){
        $roles=$this->find()->contain(['UsersGroups'])
                    ->order(['UsersGroups.nombre'=>'ASC','UsersRoles.nombre'=>'ASC'])
                    ->all();

        $retorno=[];
        foreach ($roles as $fila){
            $idGrupo=$fila['users_group']->id;
            $nombreGrupo=$fila['users_group']->nombre;
            $idRol=$fila['id'];
            $nombreRol=$fila['nombre'];

            $retorno[$nombreGrupo][$idRol]=[
                'grupo_id'=>$idGrupo,
                'grupo_nombre'=>$nombreGrupo,
                'rol_id'=>$idRol,
                'rol_nombre'=>$nombreRol
            ];
        }

        return $retorno;
    }

    public function agruparAsignadas($asignadas){
        $retorno=[];
        foreach($asignadas as $fila):
            $plugin=$fila['plugin'];
            $controller=$fila['controller'];
            $action=$fila['action'];

            $llave="$plugin/$controller";

            $retorno[$llave]['plugin']=$plugin;
            $retorno[$llave]['controller']=$controller;
            $retorno[$llave]['acciones'][]=$fila;
        endforeach;
        return $retorno;
    }

}
