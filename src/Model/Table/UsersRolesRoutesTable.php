<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersRolesRoutesTable extends Table
{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users_roles_routes');
        $this->setDisplayField('users_role_id');
        $this->setPrimaryKey(['users_role_id', 'plugin', 'controller', 'action']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('UsersRoles', [
            'foreignKey' => 'users_role_id',
            'joinType' => 'INNER'
        ]);
    }

    
    public function validationDefault(Validator $validator){
        $validator
            ->scalar('plugin')
            ->maxLength('plugin', 100)
            ->allowEmptyString('plugin', null, 'create');

        $validator
            ->scalar('controller')
            ->maxLength('controller', 100)
            ->allowEmptyString('controller', null, 'create');

        $validator
            ->scalar('action')
            ->maxLength('action', 100)
            ->allowEmptyString('action', null, 'create');

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['users_role_id'], 'UsersRoles'));

        return $rules;
    }

    public function actualizarPermisos($idRol,$permisos){

        $this->deleteAll(['users_role_id' => $idRol]);

        if(count($permisos) == 0){
            return true;
        }
        
        foreach ($permisos as $plugin => $controladores) {
            foreach ($controladores as $controlador => $rutas) {
                foreach ($rutas as $ruta => $valor) {
                    $datos[]=[
                        'users_role_id'=>$idRol,
                        'plugin'=>$plugin,
                        'controller'=>$controlador,
                        'action'=>$ruta
                    ];   
                }
            }
        }

        $entities = $this->newEntities($datos);
        return $this->saveMany($entities) ? true : false;
    }

    public function consultarAsignadas($idRol){
        $asignadas = $this->find()->where(['users_role_id' => $idRol])->all();

        $retorno=[];

        foreach ($asignadas as $fila) {
            $llave=$fila['llave'];
            $retorno[$llave]=[
                'users_role_id'=>$fila['users_role_id'],
                'plugin'=>$fila['plugin'],
                'controller'=>$fila['controller'],
                'action'=>$fila['action']
            ];
        }

        return $retorno;
    }
    
}
