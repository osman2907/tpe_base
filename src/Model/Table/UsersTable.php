<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class UsersTable extends Table{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Status', [
            'className' => 'Descripcions',
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Personas', [
            'className' => 'Personas',
            'foreignKey' => 'persona_id',
            'joinType' => 'INNER'
        ]);
    }

    
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('usuario')
            ->maxLength('usuario', 100)
            ->requirePresence('usuario', 'create')
            ->notEmptyString('usuario','Ingrese usuario.');

        $validator
            ->scalar('contrasena')
            ->maxLength('contrasena', 100)
            ->requirePresence('contrasena', 'create')
            ->notEmptyString('contrasena','Ingrese contraseña.');

        $validator
            ->requirePresence('contrasena_confirm', function(){
                if(isset($context['data']['action'])){
                    return $context['data']['action'] === 'editarContrasena';
                }
                return false;
            })
            ->notEmptyString('contrasena_confirm','Confirme contraseña.')
            ->add('contrasena_confirm','confirmacion',[
                'rule'=>function($value,$context){
                    if($context['data']['contrasena'] != $context['data']['contrasena_confirm']){
                        return "Debe coincidir con la contraseña.";
                    }
                    return true;
                }
            ]);

        $validator
            ->scalar('role')
            ->maxLength('role', 100)
            ->requirePresence('role', 'create')
            ->notEmptyString('role','Seleccione rol');

        $validator
            ->scalar('persona_id')
            ->requirePresence('persona_id', 'create');

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        $rules->add($rules->existsIn(['persona_id'], 'Personas'));

        return $rules;
    }


    public function eliminarUsuario($id){
        $user=$this->get($id);
        $user->status_id=102;
        return $this->save($user) ? true : false;
    }


    public function reciclarUsuario($id){
        $user=$this->get($id);
        $user->status_id=101;
        return $this->save($user) ? true : false;
    }


    public function validarUsuarioIdentificacion($prefijo,$cedula){
        $consulta=$this->find()->contain('Personas')->where(['Personas.prefijo'=>$prefijo,'Personas.cedula'=>$cedula])->all();
        return $consulta;
    }


    public function obtenerRutasPermitidas($idUser){
        $con = ConnectionManager::get('default');
        $query="
            SELECT urr.plugin, urr.controller, urr.action 
            FROM users us
                INNER JOIN users_roles_assignments ua ON ua.user_id=us.id
                INNER JOIN users_roles ur ON ur.id=ua.users_role_id
                INNER JOIN users_roles_routes urr ON urr.users_role_id=ur.id
            WHERE us.id=$idUser
            GROUP BY urr.plugin, urr.controller, urr.action
        ";
        $consulta = $con->execute($query);
        $permisos = $consulta->fetchAll('assoc');

        $retorno=[];

        $retorno['app']['pages']['logout']=[];
        $retorno['app']['pages']['login']=[];
        $retorno['app']['pages']['inicio']=[];

        foreach ($permisos as $fila) {
            $plugin=strtolower($fila['plugin']);
            $controller=strtolower($fila['controller']);
            $action=strtolower($fila['action']);

            $retorno[$plugin][$controller][$action]=$fila;
        }

        return $retorno;
    }


    public function asignarRutasTodas($rutas){
        $retorno=[];
        foreach ($rutas as $filaRuta) {
            $plugin=strtolower($filaRuta['plugin']);
            $controller=strtolower($filaRuta['controller']);

            foreach ($filaRuta['acciones'] as $accion){
                $action=strtolower($accion);
                $retorno[$plugin][$controller][$action]=0;
            }
        }

        return $retorno;
    }

}
