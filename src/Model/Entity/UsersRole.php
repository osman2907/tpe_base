<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsersRole Entity
 *
 * @property int $id
 * @property string $nombre
 * @property int $status_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $users_group_id
 *
 * @property \App\Model\Entity\Descripcion $descripcion
 * @property \App\Model\Entity\UsersGroup $users_group
 * @property \App\Model\Entity\UsersRolesAssignment[] $users_roles_assignments
 * @property \App\Model\Entity\UsersRolesRoute[] $users_roles_routes
 */
class UsersRole extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'status_id' => true,
        'created' => true,
        'modified' => true,
        'users_group_id' => true,
        'descripcion' => true,
        'users_group' => true,
        'users_roles_assignments' => true,
        'users_roles_routes' => true
    ];
}
