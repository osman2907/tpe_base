<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PeriodosAcademico Entity
 *
 * @property int $id
 * @property string $nombre
 * @property \Cake\I18n\FrozenDate $fecha_inicio
 * @property \Cake\I18n\FrozenDate $fecha_fin
 * @property int $estatus_id
 * @property int $status_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Descripcion $descripcion
 */
class PeriodosAcademico extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'fecha_inicio' => true,
        'fecha_fin' => true,
        'estatus_id' => true,
        'status_id' => true,
        'created' => true,
        'modified' => true,
        'descripcion' => true
    ];


}
