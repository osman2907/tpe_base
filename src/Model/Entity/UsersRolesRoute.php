<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class UsersRolesRoute extends Entity
{
    
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'users_role' => true,
        '*'=>true
    ];

    protected $_virtual = ['llave'];

    protected function _getLlave() {
        return $this->users_role_id.'~'.$this->plugin.'~'.$this->controller.'~'.$this->action;
    }
}
