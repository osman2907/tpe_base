<?php
namespace Personas\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PersonasTable extends Table{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('personas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Generos', [
            'foreignKey' => 'genero_id',
            'joinType' => 'INNER',
            'className' => 'Descripcions'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER',
            'className' => 'Descripcions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('cedula')
            ->maxLength('cedula', 20)
            ->requirePresence('cedula', 'create')
            ->notEmptyString('cedula','Ingrese cédula.')
            ->add('cedula', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>'Cédula registrada previamente']);

        $validator
            ->scalar('prefijo')
            ->maxLength('prefijo', 5)
            ->requirePresence('prefijo', 'create')
            ->notEmptyString('prefijo');

        $validator
            ->scalar('primer_nombre')
            ->maxLength('primer_nombre', 100)
            ->requirePresence('primer_nombre', 'create')
            ->notEmptyString('primer_nombre','Ingrese primer nombre.');

        $validator
            ->scalar('segundo_nombre')
            ->maxLength('segundo_nombre', 100)
            ->allowEmptyString('segundo_nombre');

        $validator
            ->scalar('primer_apellido')
            ->maxLength('primer_apellido', 100)
            ->requirePresence('primer_apellido', 'create')
            ->notEmptyString('primer_apellido','Ingrese primer apellido.');

        $validator
            ->scalar('segundo_apellido')
            ->maxLength('segundo_apellido', 100)
            ->allowEmptyString('segundo_apellido');

        $validator
            ->scalar('genero_id')
            ->requirePresence('genero_id', 'create')
            ->notEmpty('genero_id','Seleccione género.');

        $validator
            ->scalar('telefono1')
            ->maxLength('telefono1', 100)
            ->requirePresence('telefono1', 'create')
            ->notEmptyString('telefono1','Ingrese teléfono 1');

        $validator
            ->scalar('telefono2')
            ->maxLength('telefono2', 100)
            ->allowEmptyString('telefono2');

        $validator
            ->scalar('correo_electronico')
            ->maxLength('correo_electronico', 300)
            ->requirePresence('correo_electronico', 'create')
            ->notEmptyString('correo_electronico','Ingrese correo electrónico');

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['cedula']));
        $rules->add($rules->existsIn(['genero_id'], 'Generos'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }


    public function eliminarPersona($id){
        $persona=$this->get($id);
        $persona->status_id=102;
        return $this->save($persona) ? true : false;
    }


    public function reciclarPersona($id){
        $persona=$this->get($id);
        $persona->status_id=101;
        return $this->save($persona) ? true : false;
    }

    public function validarPersonaIdentificacion($prefijo,$cedula){
        $consulta=$this->find()->where(['prefijo'=>$prefijo,'cedula'=>$cedula])->all();
        return $consulta->toArray();
    }

}
