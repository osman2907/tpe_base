<?php
namespace Personas\Model\Entity;

use Cake\ORM\Entity;

class Persona extends Entity{
    
    protected $_accessible = [
        'cedula' => true,
        'prefijo' => true,
        'primer_nombre' => true,
        'segundo_nombre' => true,
        'primer_apellido' => true,
        'segundo_apellido' => true,
        'genero_id' => true,
        'telefono1' => true,
        'telefono2' => true,
        'correo_electronico' => true,
        'status_id' => true,
        'created' => true,
        'modified' => true,
        'descripcion' => true
    ];

    protected $_virtual = ['nombre_apellido'];

    protected function _getNombreApellido() {
        if(isset($this->_properties['primer_nombre']) && isset($this->_properties['primer_apellido'])){
            return $this->_properties['primer_nombre'] . ' ' . $this->_properties['primer_apellido'];
        }
        return "";
    }
}
