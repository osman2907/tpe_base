<?php
namespace Personas\Controller;

use Personas\Controller\AppController;

class PersonasController extends AppController{

    public function listar(){
        if(isset($this->request['data']['filtros']) || isset($this->request->query['sort'])){
            $this->paginate = [
                'contain' => ['Status', 'Generos'],
                'sortWhitelist' => ['id','cedula','primer_nombre','primer_apellido','Generos.nombre'],
                'limit' => 10
            ];

            if(!empty($this->request->data['filtros']['prefijo'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Personas.prefijo' => $this->request->data['filtros']['prefijo']]];
            }

            if(!empty($this->request->data['filtros']['cedula'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Personas.cedula' => $this->request->data['filtros']['cedula']]];
            }

            if(!empty($this->request->data['filtros']['primer_nombre'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(Personas.primer_nombre) LIKE' =>  '%'.strtolower($this->request->data['filtros']['primer_nombre']).'%']];
            }

            if(!empty($this->request->data['filtros']['primer_apellido'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(Personas.primer_apellido) LIKE' =>  '%'.strtolower($this->request->data['filtros']['primer_apellido']).'%']];
            }

            if(!empty($this->request->data['filtros']['genero_id'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Personas.genero_id' => $this->request->data['filtros']['genero_id']]];
            }

            if(!empty($this->request->data['filtros']['mostrar'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Personas.status_id' => $this->request->data['filtros']['mostrar']]];
            }
            $personas = $this->paginate($this->Personas);

            $this->set(compact('personas'));
            $this->render("listar_personas");
        }

        if(!isset($this->request['data']['filtros'])){
            $personas = $this->Personas;
            $generosList = $this->Personas->Generos->find('list', ['limit' => 200])->where(['padre_id'=>'10']);
            $prefijosList = $this->listarPrefijosPersonas();
            $this->set(compact('personas','generosList','prefijosList'));
        }
    }

    
    public function consultar($id = null){
        $persona = $this->Personas->get($id, [
            'contain' => ['Generos','Status']
        ]);

        $this->set('persona', $persona);
    }


    public function registrar(){
        $persona = $this->Personas->newEntity();

        if (isset($this->request['data']['filtros'])){
            $persona->status_id=101; //Activo por defecto

            $generosList = $this->Personas->Generos->find('list', ['limit' => 200])->where(['padre_id'=>'10']);
            $prefijosList = $this->listarPrefijosPersonas();
            $this->set(compact('persona','generosList','prefijosList'));
        }

        if (!isset($this->request['data']['filtros'])){
            if ($this->request->is('post')) {
                $persona = $this->Personas->patchEntity($persona, $this->request->getData());
                if(count($persona->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($persona->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Personas->save($persona)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Persona registrada exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error registrando persona']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando persona']);
            }
            exit();
        }
    }

    public function editar($id = null){
        $persona = $this->Personas->get($id,['contain' => ['Status','Generos']]);

        if (isset($this->request['data']['filtros'])){
            $generosList = $this->Personas->Generos->find('list', ['limit' => 200])->where(['padre_id'=>'10']);
            $prefijosList = $this->listarPrefijosPersonas();
            $this->set(compact('persona','generosList','prefijosList'));
        }

        if (!isset($this->request['data']['filtros'])){
            $persona = $this->Personas->get($id);
            if ($this->request->is('put')){
                $persona = $this->Personas->patchEntity($persona, $this->request->getData());
                if(count($persona->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($persona->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Personas->save($persona)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Persona editada exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error editando persona']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando persona']);
            }
            exit();
        }

    }


    public function eliminar($id){
        $eliminar=$this->Personas->eliminarPersona($id);
        if($eliminar){
            echo json_encode(['result'=>true, 'mensaje'=>'Persona eliminada con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        $this->autoRender = false;
    }


    public function reciclar($id){
        $reciclar=$this->Personas->reciclarPersona($id);
        if($reciclar){
            echo json_encode(['result'=>true, 'mensaje'=>'Persona reciclada con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        $this->autoRender = false;
    }
    
}
