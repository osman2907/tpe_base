<div class="modal fade" id="modal-consultar-persona" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Consultar persona</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <tr class="table-active">
                                <th>Cedula</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                            </tr>

                            <tr>
                                <td><?= strtoupper($persona->prefijo)."-".$persona->cedula; ?></td>
                                <td><?= $persona->primer_nombre." ".$persona->segundo_nombre; ?></td>
                                <td><?= $persona->primer_apellido." ".$persona->segundo_apellido; ?></td>
                            </tr>

                            <tr>
                                <th>Género</th>
                                <th>Teléfono 1</th>
                                <th>Teléfono 2</th>
                            </tr>

                            <tr>
                                <td><?= $persona->genero->nombre; ?></td>
                                <td><?= $persona->telefono1; ?></td>
                                <td><?= $persona->telefono2; ?></td>
                            </tr>

                            <tr>
                                <th>Correo electrónico</th>
                                <th colspan="2">Eliminado</th>
                            </tr>

                            <tr>
                                <td><?= $persona->correo_electronico; ?></td>
                                <td colspan="2">
                                    <?php
                                    $clase=$persona->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                                    ?>
                                    <h4><span class="<?= $clase ?>"><?= $persona->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-consultar-persona").modal('show');
    });
</script>
