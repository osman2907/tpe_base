<div class="table-responsive table-responsive-data2">
    <table class="table table-data2 table-hover">
        <thead>
            <tr>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('cedula') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('primer_nombre') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('primer_apellido') ?></th>
                <th scope="col" class="sorter"><?= $this->Paginator->sort('Generos.nombre','Género') ?></th>
                <th scope="col" class="sorter">Eliminado</th>
                <th scope="col" class="actions encabezado-fijo"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($personas as $persona): ?>
            <tr>
                <td><?= $this->Number->format($persona->id); ?></td>
                <td style="white-space: nowrap;"><?= strtoupper($persona->prefijo)."-".$persona->cedula; ?></td>
                <td><?= $persona->primer_nombre; ?></td>
                <td><?= $persona->primer_apellido; ?></td>
                <td><?= $persona->genero->nombre ?></td>
                <td>
                    <?php
                    $clase=$persona->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                    ?>
                    <h4><span class="<?= $clase ?>"><?= $persona->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                </td>
                <td class="actions columna-fija" style="white-space: nowrap;">
                    <?php if($persona->status_id == 101){ ?>
                        <button type="button" class="btn btn-info btn-sm btn-consultar" data-url="<?= $this->Url->build(["action" => "consultar", $persona->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Consultar" data-original-title="Consultar"><i class="fa fa-eye"></i></button>
                        <button type="button" class="btn btn-primary btn-sm btn-editar" data-url="<?= $this->Url->build(["action" => "editar", $persona->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                    <?php } ?>

                    <?php if($persona->status_id == 101){ ?>
                        <button type="button" class="btn btn-danger btn-sm btn-eliminar" data-toggle="tooltip" data-placement="bottom" title="Eliminar" data-original-title="Eliminar" data-url="<?= $this->Url->build(["action" => "eliminar", $persona->id]); ?>"><i class="fa fa-trash"></i></button>
                    <?php } ?>

                    <?php if($persona->status_id == 102){ ?>
                        <button type="button" class="btn btn-success btn-sm btn-reciclar" data-toggle="tooltip" data-placement="bottom" title="Reciclar" data-original-title="Reciclar" data-url="<?= $this->Url->build(["action" => "reciclar", $persona->id]); ?>"><i class="fa fa-recycle"></i></button>
                    <?php } ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Página <b>{{page}}</b> de <b>{{pages}}</b>, mostrando <b>{{current}}</b> registros de <b>{{count}}</b> en total')]) ?></p>
</div>

<script type="text/javascript">
    $(".sorter a, .pagination a").click(function() {
        if (!$(this).attr('href')){
            return false;
        }

        href=$(this).attr('href');
        
        $.ajax({
            type: "POST",
            url: href,
            dataType: "html",
            data:$("#listado-personas").serialize(),
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#container-listado").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
               $(".page-loader").addClass('hide');
               Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });

        return false;
    });


    $(".btn-consultar").click(function(){
        url=$(this).data('url');
        data=$("#listado-personas").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-editar").click(function(){
        url=$(this).data('url');
        data=$("#listado-personas").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-permisos").click(function(){
        url=$(this).data('url');
        data=$("#listado-personas").serialize();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "html",
            data:data,
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                $(".page-loader").removeClass('hide');
            },
            success: function(data){
                $("#modal-container").html(data);
                $(".page-loader").addClass('hide');
            },
            error: function (status, error, datos){
                $(".page-loader").addClass('hide');
                Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
            }
        });
    });


    $(".btn-eliminar").click(function(){
        url=$(this).data('url');

        Swal.fire({
            title: '',
            text: "¿Seguro desea eliminar esta persona?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value){
                data=$("#listado-personas").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data:data,
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        $(".page-loader").removeClass('hide');
                    },
                    success: function(data){
                        $(".page-loader").addClass('hide');
                        if(data.result){
                            Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                            .then((result) => {
                                if(result.value){
                                    selPage=$(".page-number[tabindex='-1']");
                                    if(selPage.length === 0){
                                        $("#buscar").trigger('click');
                                    }else{
                                        selPage.trigger('click');
                                    }
                                }
                            });
                        }else{
                            Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                        }
                    },
                    error: function (status, error, datos){
                        $(".page-loader").addClass('hide');
                        Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                    }
                });
            }
        });
    });


    $(".btn-reciclar").click(function(){
        url=$(this).data('url');

        Swal.fire({
            title: '',
            text: "¿Seguro desea reciclar esta persona?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Reciclar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value){
                data=$("#listado-personas").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data:data,
                    beforeSend: function(xhr){
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        $(".page-loader").removeClass('hide');
                    },
                    success: function(data){
                        $(".page-loader").addClass('hide');
                        if(data.result){
                            Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                            .then((result) => {
                                if(result.value){
                                    selPage=$(".page-number[tabindex='-1']");
                                    if(selPage.length === 0){
                                        $("#buscar").trigger('click');
                                    }else{
                                        selPage.trigger('click');
                                    }
                                }
                            });
                        }else{
                            Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                        }
                    },
                    error: function (status, error, datos){
                        $(".page-loader").addClass('hide');
                        Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                    }
                });
            }
        });
    });

</script>