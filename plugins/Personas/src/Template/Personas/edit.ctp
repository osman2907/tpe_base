<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $persona
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $persona->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $persona->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Personas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Descripcions'), ['controller' => 'Descripcions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Descripcion'), ['controller' => 'Descripcions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="personas form large-9 medium-8 columns content">
    <?= $this->Form->create($persona) ?>
    <fieldset>
        <legend><?= __('Edit Persona') ?></legend>
        <?php
            echo $this->Form->control('cedula');
            echo $this->Form->control('prefijo');
            echo $this->Form->control('primer_nombre');
            echo $this->Form->control('segundo_nombre');
            echo $this->Form->control('primer_apellido');
            echo $this->Form->control('segundo_apellido');
            echo $this->Form->control('genero_id');
            echo $this->Form->control('telefono1');
            echo $this->Form->control('telefono2');
            echo $this->Form->control('correo_electronico');
            echo $this->Form->control('status_id', ['options' => $descripcions]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
