<div class="modal fade" id="modal-nueva-persona" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<?= $this->Form->create($persona,['id'=>'form-nueva-persona']) ?>
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Nueva persona</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
						<div class="row">
							<div class="col-lg-2">
	              				<?= $this->Form->control('prefijo',['class'=>'form-control','options'=>$prefijosList]); ?>
	                		</div>

							<div class="col-lg-2">
	              				<?= $this->Form->control('cedula',['class'=>'form-control','label'=>'Cédula']); ?>
	                		</div>

	              			<div class="col-lg-4">
	              				<?= $this->Form->control('primer_nombre',['class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('segundo_nombre',['class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('primer_apellido',['class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('segundo_apellido',['class'=>'form-control']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('genero_id',['options'=>$generosList, 'class'=>'form-control', 'label'=>'Género', 'empty'=>'Seleccione un género']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('telefono1',['class'=>'form-control', 'label'=>'Teléfono 1']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('telefono2',['class'=>'form-control', 'label'=>'Teléfono 2']); ?>
	                		</div>

	                		<div class="col-lg-4">
	              				<?= $this->Form->control('correo_electronico',['class'=>'form-control', 'label'=>'Correo Electrónico']); ?>
	                		</div>

	                	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<?= $this->Form->button("Registrar",['id'=>'btn-registrar','class'=>'btn btn-success']); ?>
				</div>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

<script type="text/javascript">
	$("document").ready(function(){

		$("#modal-nueva-persona").modal('show');

  		$("#btn-registrar").click(function(){
  			data=$("#form-nueva-persona").serialize();
  			$.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["action" => "registrar"]); ?>",
                data:data,
                dataType: "json",
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                	$(".page-loader").addClass('hide');
                	$("label[class='error']").remove();
                	if(data.errors){
                		errores=data.errors;
                		$.each(errores, function(i,mensaje){
                			$("[name='"+i+"']").parent().append('<label class="error">'+mensaje+'</label>');
                		});
                		return false;
                	}

                	$(".close").trigger("click");
                	if(data.result){
                		Swal.fire({
				            icon: 'success',
				            title: 'Registro exitoso',
				            text: '',
				            footer: ''
				        }).then((result) => {
				        	if(result.value){
				        		selPage=$(".page-number[tabindex='-1']");
				        		if(selPage.length === 0){
				        			$("#buscar").trigger('click');
				        		}else{
				        			selPage.trigger('click');
				        		}
				        	}
				        });
                	}else{
                		Swal.fire({
				            icon: 'error',
				            title: '',
				            text: 'Error al registrar, por favor intente nuevamente',
				            footer: ''
				        });
                	}
                },
                error: function (status, error, datos){
                	$(".page-loader").addClass('hide');
                	Swal.fire({
			            icon: 'error',
			            title: '',
			            text: 'Error al registrar, por favor intente nuevamente',
			            footer: ''
			        });
                }
            });
  			return false;
  		});

	});
</script>