<?php
namespace Empresas\Model\Entity;

use Cake\ORM\Entity;

/**
 * Empresa Entity
 *
 * @property int $id
 * @property string $prefijo
 * @property string $rif
 * @property string $razon_social
 * @property string|null $objeto
 * @property int $tipo_id
 * @property string $telefono1
 * @property string|null $telefono2
 * @property string $correo_electronico
 * @property int $status_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \Empresas\Model\Entity\Descripcion $descripcion
 * @property \Empresas\Model\Entity\Status $status
 */
class Empresa extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'prefijo' => true,
        'rif' => true,
        'razon_social' => true,
        'objeto' => true,
        'tipo_id' => true,
        'telefono1' => true,
        'telefono2' => true,
        'correo_electronico' => true,
        'status_id' => true,
        'created' => true,
        'modified' => true,
        'descripcion' => true,
        'status' => true
    ];
}
