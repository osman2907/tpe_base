<?php
namespace Empresas\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class EmpresasTable extends Table{
    
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('empresas');
        $this->setDisplayField('razon_social');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER',
            'className' => 'Descripcions'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER',
            'className' => 'Descripcions'
        ]);
    }

    
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('prefijo')
            ->maxLength('prefijo', 10)
            ->requirePresence('prefijo', 'create')
            ->notEmptyString('prefijo');

        $validator
            ->scalar('rif')
            ->maxLength('rif', 20)
            ->requirePresence('rif', 'create')
            ->notEmptyString('rif')
            ->add('rif', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('razon_social')
            ->maxLength('razon_social', 300)
            ->requirePresence('razon_social', 'create')
            ->notEmptyString('razon_social');

        $validator
            ->scalar('objeto')
            ->allowEmptyString('objeto');

        $validator
            ->scalar('telefono1')
            ->maxLength('telefono1', 100)
            ->requirePresence('telefono1', 'create')
            ->notEmptyString('telefono1');

        $validator
            ->scalar('telefono2')
            ->maxLength('telefono2', 100)
            ->allowEmptyString('telefono2');

        $validator
            ->scalar('correo_electronico')
            ->maxLength('correo_electronico', 300)
            ->requirePresence('correo_electronico', 'create')
            ->notEmptyString('correo_electronico');

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['rif']));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }


    public function eliminarEmpresa($id){
        $empresa=$this->get($id);
        $empresa->status_id=102;
        return $this->save($empresa) ? true : false;
    }


    public function reciclarEmpresa($id){
        $empresa=$this->get($id);
        $empresa->status_id=101;
        return $this->save($empresa) ? true : false;
    }
    
}
