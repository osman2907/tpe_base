<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $empresa
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $empresa->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $empresa->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Empresas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Descripcions'), ['controller' => 'Descripcions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Descripcion'), ['controller' => 'Descripcions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="empresas form large-9 medium-8 columns content">
    <?= $this->Form->create($empresa) ?>
    <fieldset>
        <legend><?= __('Edit Empresa') ?></legend>
        <?php
            echo $this->Form->control('prefijo');
            echo $this->Form->control('rif');
            echo $this->Form->control('razon_social');
            echo $this->Form->control('objeto');
            echo $this->Form->control('tipo_id', ['options' => $descripcions]);
            echo $this->Form->control('telefono1');
            echo $this->Form->control('telefono2');
            echo $this->Form->control('correo_electronico');
            echo $this->Form->control('status_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
