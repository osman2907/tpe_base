<div class="modal fade" id="modal-consultar-empresa" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Consultar empresa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <tr class="table-active">
                                <th>RIF</th>
                                <th>Razón social</th>
                                <th>Tipo de empresa</th>
                            </tr>

                            <tr>
                                <td><?= strtoupper($empresa->prefijo)."-".$empresa->rif; ?></td>
                                <td><?= $empresa->razon_social; ?></td>
                                <td><?= $empresa->tipo->nombre; ?></td>
                            </tr>

                            <tr>
                                <th>Teléfono 1</th>
                                <th>Teléfono 2</th>
                                <th>Correo electrónico</th>
                            </tr>

                            <tr>
                                <td><?= $empresa->telefono1; ?></td>
                                <td><?= $empresa->telefono2; ?></td>
                                <td><?= $empresa->correo_electronico; ?></td>
                            </tr>

                            <tr>
                                <th colspan="3">Eliminado</th>
                            </tr>

                            <tr>
                                <td colspan="3">
                                    <?php
                                    $clase=$empresa->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                                    ?>
                                    <h4><span class="<?= $clase ?>"><?= $empresa->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("document").ready(function(){
        $("#modal-consultar-empresa").modal('show');
    });
</script>
