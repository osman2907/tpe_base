<div class="users-groups index large-9 medium-8 columns content">
    <?= $this->Form->create($empresas,['id'=>'listado-empresas']) ?>
        <h3><?= __('Empresas') ?></h3>
        <br>
        <?= $this->Html->link(__('Nueva empresa'), ['action' => 'add'],['id'=>'btn-nueva-empresa', 'class'=>'btn btn-success']) ?>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2 table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('rif') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('razon_social') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('tipo_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status_id','Eliminado') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($empresas as $empresa): ?>
                    <tr>
                        <td><?= $this->Number->format($empresa->id); ?></td>
                        <td style="white-space: nowrap;"><?= strtoupper($empresa->prefijo)."-".$empresa->rif; ?></td>
                        <td><?= $empresa->razon_social; ?></td>
                        <td><?= $empresa->tipo->nombre; ?></td>
                        <td>
                            <?php
                            $clase=$empresa->status_id == 101 ? 'badge badge-success' : 'badge badge-danger';
                            ?>
                            <h4><span class="<?= $clase ?>"><?= $empresa->status_id == 101 ? 'No' : 'Si'; ?></span></h4>
                        </td>
                        <td class="actions" style="white-space: nowrap;">
                            <?php if($empresa->status_id == 101){ ?>
                                <button type="button" class="btn btn-info btn-sm btn-consultar" data-url="<?= $this->Url->build(["action" => "view", $empresa->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Consultar" data-original-title="Consultar"><i class="fa fa-eye"></i></button>
                                <button type="button" class="btn btn-primary btn-sm btn-modificar" data-url="<?= $this->Url->build(["action" => "editarEmpresa", $empresa->id]); ?>" data-toggle="tooltip" data-placement="bottom" title="Editar" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                            <?php } ?>

                            <?php if($empresa->status_id == 101){ ?>
                                <button type="button" class="btn btn-danger btn-sm btn-eliminar" data-toggle="tooltip" data-placement="bottom" title="Eliminar" data-original-title="Eliminar" data-url="<?= $this->Url->build(["action" => "eliminarEmpresa", $empresa->id]); ?>"><i class="fa fa-trash"></i></button>
                            <?php } ?>

                            <?php if($empresa->status_id == 102){ ?>
                                <button type="button" class="btn btn-success btn-sm btn-reciclar" data-toggle="tooltip" data-placement="bottom" title="Reciclar" data-original-title="Reciclar" data-url="<?= $this->Url->build(["action" => "reciclarEmpresa", $empresa->id]); ?>"><i class="fa fa-recycle"></i></button>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página <b>{{page}}</b> de <b>{{pages}}</b>, mostrando <b>{{current}}</b> registros de <b>{{count}}</b> en total')]) ?></p>
        </div>
    <?= $this->Form->end() ?>
    
</div>


<script type="text/javascript">
    $(document).ready(function(){

        $("#btn-nueva-empresa").click(function(){
            data=$("#listado-empresas").serialize();
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "empresas", "action" => "nuevaEmpresa"]); ?>",
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        });


        $(".btn-consultar").click(function(){
            url=$(this).data('url');
            data=$("#listado-empresas").serialize();
            $.ajax({
                type: "POST",
                url: url,
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                    $(".page-loader").addClass('hide');
                    Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
        });


        $(".btn-modificar").click(function(){
            url=$(this).data('url');
            data=$("#listado-empresas").serialize();
            $.ajax({
                type: "POST",
                url: url,
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                    $(".page-loader").addClass('hide');
                    Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
        });


        $(".btn-eliminar").click(function(){
            url=$(this).data('url');

            Swal.fire({
                title: '',
                text: "¿Seguro desea eliminar esta empresa?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#listado-empresas").serialize();
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                                .then((result) => {
                                    if(result.value){
                                        $(".page-loader").removeClass('hide');
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
        });


        $(".btn-reciclar").click(function(){
            url=$(this).data('url');

            Swal.fire({
                title: '',
                text: "¿Seguro desea reciclar esta empresa?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Reciclar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value){
                    data=$("#listado-empresas").serialize();
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: "json",
                        data:data,
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            $(".page-loader").removeClass('hide');
                        },
                        success: function(data){
                            $(".page-loader").addClass('hide');
                            if(data.result){
                                Swal.fire({icon: 'success',title: '',text: data.mensaje, footer: ''})
                                .then((result) => {
                                    if(result.value){
                                        $(".page-loader").removeClass('hide');
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({icon: 'error',title: '',text: data.mensaje, footer: ''});
                            }
                        },
                        error: function (status, error, datos){
                            $(".page-loader").addClass('hide');
                            Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                        }
                    });
                }
            });
        });

    });
</script>
