<div class="users-groups index large-9 medium-8 columns content">
    <?= $this->Form->create($empresas,['id'=>'listado-empresas']) ?>
        <h3><?= __('Empresas') ?></h3>
        <br>
        <?= $this->Html->link(__('Nueva empresa'), ['action' => 'add'],['id'=>'btn-nueva-empresa', 'class'=>'btn btn-success']) ?>

        <div id="container-filtros">
            <div class="row">
                <div class="col-sm-12 text-center">
                    Filtros listado de empresas
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 col-md-6 col-lg-2">
                    <?php
                    echo $this->Form->label("prefijo","Prefijo",['style'=>'display: block']); 
                    echo $this->Form->select('filtros[prefijo]',$prefijosList,['class'=>'form-control-sm','empty'=>'Todos','style'=>'width:100%']);
                    ?>
                </div>

                <div class="col-sm-8 col-md-6 col-lg-2">
                    <?php
                    echo $this->Form->label("rif","RIF",['style'=>'display: block']); 
                    echo $this->Form->text('filtros[rif]',['class'=>'form-control-sm solo-numero','style'=>'width:100%']); 
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    echo $this->Form->label("razon_social","Razón social",['style'=>'display: block']); 
                    echo $this->Form->text('filtros[razon_social]',['class'=>'form-control-sm','style'=>'width:100%']); 
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    echo $this->Form->label("primer_apellido","Apellido",['style'=>'display: block']); 
                    echo $this->Form->text('filtros[primer_apellido]',['class'=>'form-control-sm','style'=>'width:100%']); 
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    echo $this->Form->label("tipo_id","Tipo empresa",['style'=>'display: block']); 
                    echo $this->Form->select('filtros[tipo_id]',$tiposList,['class'=>'form-control-sm','empty'=>'Seleccione tipo empresa','style'=>'width:100%']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <?php
                    $mostrarList=[101=>'Registros activos',102=>'Registros eliminados'];
                    echo $this->Form->label("mostrar","Mostrar",['style'=>'display: block']); 
                    echo $this->Form->select('filtros[mostrar]',$mostrarList,['class'=>'form-control-sm','style'=>'width:100%','empty'=>'Todos','value'=>101]); 
                    ?>
                </div>
            </div>

            <div class="row" style="margin-top: 20px;">
                <div class="col-sm-6 col-md-4 col-lg-2">
                    <button id="buscar" type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-search"></i>&nbsp; Buscar
                    </button>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-2">
                    <button type="reset" class="btn btn-warning btn-sm">
                        <i class="fa fa-eraser"></i>&nbsp; Limpiar
                    </button>
                </div>
            </div>
        </div>

        <br>

        <div id="container-listado"></div>
        
    <?= $this->Form->end() ?>

</div>


<script type="text/javascript">
    $(document).ready(function(){

        function cargar_tabla(){
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "empresas", "action" => "listar"]); ?>",
                dataType: "html",
                data:$("#listado-empresas").serialize(),
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#container-listado").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        }
        cargar_tabla();


        $("#btn-nueva-empresa").click(function(){
            data=$("#listado-empresas").serialize();
            $.ajax({
                type: "POST",
                url: "<?= $this->Url->build(["controller" => "empresas", "action" => "registrar"]); ?>",
                dataType: "html",
                data:data,
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    $(".page-loader").removeClass('hide');
                },
                success: function(data){
                    $("#modal-container").html(data);
                    $(".page-loader").addClass('hide');
                },
                error: function (status, error, datos){
                   $(".page-loader").addClass('hide');
                   Swal.fire({icon: 'error',title: '',text: 'Ha ocurrido un error, por favor intente nuevamente',footer: ''});
                }
            });
            return false;
        });


        $("#buscar").click(function(){
            cargar_tabla();
            return false;
        });


    });
</script>
