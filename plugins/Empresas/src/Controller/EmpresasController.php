<?php
namespace Empresas\Controller;

use Empresas\Controller\AppController;

class EmpresasController extends AppController{
    
    /*public function index(){
        $this->paginate = [
            'contain' => ['Tipos', 'Status']
        ];
        $empresas = $this->paginate($this->Empresas);

        $this->set(compact('empresas'));
    }*/

    public function listar(){
        if(isset($this->request['data']['filtros']) || isset($this->request->query['sort'])){
            $this->paginate = [
                'contain' => ['Status','Tipos'],
                'sortWhitelist' => ['id','prefijo','rif','razon_social','Tipos.nombre'],
                'limit' => 10
            ];

            if(!empty($this->request->data['filtros']['prefijo'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Empresas.prefijo' => $this->request->data['filtros']['prefijo']]];
            }

            if(!empty($this->request->data['filtros']['rif'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Empresas.rif' => $this->request->data['filtros']['rif']]];
            }

            if(!empty($this->request->data['filtros']['razon_social'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['LOWER(Empresas.razon_social) LIKE' =>  '%'.strtolower($this->request->data['filtros']['razon_social']).'%']];
            }

            if(!empty($this->request->data['filtros']['tipo_id'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Empresas.tipo_id' => $this->request->data['filtros']['tipo_id']]];
            }

            if(!empty($this->request->data['filtros']['mostrar'])){
                $this->paginate['conditions'][] = [ 
                    'AND' => ['Empresas.status_id' => $this->request->data['filtros']['mostrar']]];
            }
            $empresas = $this->paginate($this->Empresas);

            $this->set(compact('empresas'));
            $this->render("listar_empresas");
        }

        if(!isset($this->request['data']['filtros'])){
            $empresas = $this->Empresas;
            $tiposList = $this->Empresas->Tipos->find('list', ['limit' => 200])->where(['padre_id'=>'15']);
            $prefijosList = $this->listarPrefijosEmpresas();
            $this->set(compact('empresas','tiposList','prefijosList'));
        }
    }

    
    public function consultar($id = null){
        $empresa = $this->Empresas->get($id, [
            'contain' => ['Tipos', 'Status']
        ]);

        $this->set('empresa', $empresa);
    }


    public function registrar(){
        $empresa = $this->Empresas->newEntity();

        if (isset($this->request['data']['filtros'])){
            $empresa->status_id=101; //Activo por defecto

            $tiposList = $this->Empresas->Tipos->find('list', ['limit' => 200])->where(['padre_id'=>'15']);
            $prefijosList = $this->listarPrefijosEmpresas();
            $this->set(compact('empresa','tiposList','prefijosList'));
        }

        if (!isset($this->request['data']['filtros'])){
            if ($this->request->is('post')) {
                $empresa = $this->Empresas->patchEntity($empresa, $this->request->getData());
                if(count($empresa->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($empresa->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Empresas->save($empresa)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Empresa registrada exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error registrando empresa']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error registrando empresa']);
            }
            exit();
        }
    }


    public function editar($id = null){
        $empresa = $this->Empresas->get($id,['contain' => ['Status','Tipos']]);

        if (isset($this->request['data']['filtros'])){
            $tiposList = $this->Empresas->Tipos->find('list', ['limit' => 200])->where(['padre_id'=>'15']);
            $prefijosList = $this->listarPrefijosEmpresas();
            $this->set(compact('empresa','tiposList','prefijosList'));
        }

        if (!isset($this->request['data']['filtros'])){
            $empresa = $this->Empresas->get($id);
            if ($this->request->is('put')){
                $empresa = $this->Empresas->patchEntity($empresa, $this->request->getData());
                if(count($empresa->errors()) > 0){ //Validación a través del modelo...
                    $errores=$this->formatearErrores($empresa->errors());
                    echo json_encode(['result'=>false, 'errors' => $errores]);
                    exit();
                }
                if ($this->Empresas->save($empresa)){
                    echo json_encode(['result'=>true, 'mensaje'=>'Empresa editada exitosamente']);
                }else{
                    echo json_encode(['result'=>false, 'mensaje'=>'Error editando empresa']);
                }
            }else{
                echo json_encode(['result'=>false, 'mensaje'=>'Error editando empresa']);
            }
            exit();
        }

    }


    public function eliminar($id){
        $eliminar=$this->Empresas->eliminarEmpresa($id);
        if($eliminar){
            echo json_encode(['result'=>true, 'mensaje'=>'Empresa eliminada con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


    public function reciclar($id){
        $reciclar=$this->Empresas->reciclarEmpresa($id);
        if($reciclar){
            echo json_encode(['result'=>true, 'mensaje'=>'Empresa reciclada con éxito']);
        }else{
            echo json_encode(['result'=>false, 'mensaje'=>'Ha ocurrido un error, por favor intente nuevamente']);
        }
        exit();
    }


}
