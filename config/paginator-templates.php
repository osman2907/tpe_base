<?php
$config = [
    'number' => '<li><a href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="active"><a href="#">{{text}}</a></li>',
    'nextActive' => '<li><a aria-label="Next" href="{{url}}">{{text}}</a></li>',
    'nextDisabled' => '<li class="next disabled"><a><span aria-hidden="true">Siguiente</span></a></li>',
    'prevActive' => '<li><a aria-label="Previous" href="{{url}}">{{text}}</a></li>',
    'prevDisabled' => '<li class="prev disabled"><a aria-label="Previous"><span aria-hidden="true">Anterior</span></a></li>'
];


$config = [
    'number' => '<li class="page-item"><a class="page-link page-number" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="page-item disabled"><a class="page-link page-number" href="{{url}}" tabindex="-1">{{text}}</a></li>',
    'nextActive' => '
        <li class="page-item d-none d-sm-none d-md-block"><a class="page-link" href="{{url}}">Siguiente</a></li>
        <li class="page-item d-block d-sm-block d-md-none"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right"></i></a></li>
    ',
    'nextDisabled' => '
        <li class="page-item disabled d-block d-sm-block d-md-none"><a class="page-link" href="#" tabindex="-1"><i class="fa fa-angle-right"></i></a></li>
        <li class="page-item disabled d-none d-sm-none d-md-block"><a class="page-link" href="#" tabindex="-1">Siguiente</a></li>
    ',
    'prevActive' => '
        <li class="page-item d-none d-sm-none d-md-block"><a class="page-link" href="{{url}}">Anterior</a></li>
        <li class="page-item d-block d-sm-block d-md-none"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left"></i></a></li>
    ',
    'prevDisabled' => '
        <li class="page-item disabled d-none d-sm-none d-md-block"><a class="page-link" href="#" tabindex="-1">Anterior</a></li>
        <li class="page-item disabled d-block d-sm-block d-md-none"><a class="page-link" href="#" tabindex="-1"><i class="fa fa-angle-left"></i></a></li>',
    'last' => '
        <li class="page-item d-block d-sm-block d-md-none"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-right"></i></a></li>
        <li class="page-item d-none d-sm-none d-md-block"><a class="page-link" href="{{url}}">Última</a></li>
    ',
    'first' => '
        <li class="page-item d-none d-sm-none d-md-block"><a class="page-link" href="{{url}}">Primera</a></li>
        <li class="page-item d-block d-sm-block d-md-none"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-left"></i></a></li>
    '
];
?>