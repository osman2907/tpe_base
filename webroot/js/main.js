$(document).ready(function(){
	
	$("#link-cerrar-sesion").click(function(){
		url=$(this).attr('href');
		Swal.fire({
            title: '',
            text: "¿Seguro desea cerrar sesión?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.value){
            	$(".page-loader").removeClass('hide');
            	document.location=url
            }
        });
        return false;
	});


	$("#formulario-busqueda").submit(function(){
		Swal.fire({
            title: '',
            text: "En construcción",
            icon: 'warning'
        })
		return false;
	});


	$(document).ajaxComplete(function(e, xhr, settings){
		if(xhr.status == 201){
			Swal.fire({icon: 'error',title: '',text: 'Su sesión ha vencido, por favor ingrese nuevamente',footer: ''}).then((result) => {
				if(result.value){
					location.reload();
                }
			});
		}

	});

	$(document).on('keyup','.solo-numero',function (){
	    this.value = (this.value + '').replace(/[^0-9]/g,'');
	});


	$(".btn-ocultar-filtros").click(function(){
		$(".btn-mostrar-filtros").removeClass('hide');
		$(".btn-ocultar-filtros").addClass('hide');

		$(".filtros-cuerpo, .filtros-pie").addClass('hide');
		return false;
	});

	$(".btn-mostrar-filtros").click(function(){
		$(".btn-ocultar-filtros").removeClass('hide');
		$(".btn-mostrar-filtros").addClass('hide');

		$(".filtros-cuerpo, .filtros-pie").removeClass('hide');
		return false;
	});


	//Adaptación para que el selector contains no sea sensible a minúsculas y mayúsculas...
	jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg){
	    return function( elem ) {
	        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	    };
	});

});


$.datepicker.regional['es'] = {
	closeText: 'Cerrar',
	prevText: 'Anterior',
	nextText: 'Siguiente',
	currentText: 'Hoy',
	monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	weekHeader: 'Sm',
	dateFormat: 'dd/mm/yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: '',
	calendarWeeks: true,
	changeMonth: true,
	changeYear: true
};
$.datepicker.setDefaults($.datepicker.regional['es']);
